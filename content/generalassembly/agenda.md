---
title: "Agenda General Assembly KDE e.V. 2023"
layout: page
---

This is the agenda for the Annual General Assembly (AGM) of KDE e.V. for 2023.
It takes place Online on [Saturday 9th of September at 1 pm UTC](https://zonestamp.toolforge.org/1694264439) and in-person at KDE e.V.'s office in Berlin (extremely limited capacity, please register with the board in advance if you want to be there in person).


## Agenda

1. Welcome
2. Election of the chair of the general assembly
3. Report of the board
   1. Report about activities
   2. Report of the treasurer
   3. Report of the auditors of accounting
   4. Relief of the board
4. Report of representatives, working groups and task forces of KDE e.V.
   1. Report of the representatives to the KDE Free Qt Foundation
   2. Report of the KDE Free Qt Working Group
   3. Report of the System Administration Working Group
   4. Report of the Community Working Group
   5. Report of the Financial Working Group
   6. Report of the Advisory Board Working Group
   7. Report of the Fundraising Working Group
5. Election of the board 
6. Election of the Auditors of Accounting
7. Election of the Representatives to the KDE Free Qt Foundation
8. Vote on allowing fully virtual AGMs (as per § 32 BGB)
9. Votes on updating the rules of prodedure for online votes
   1. Vote on changing ballot notification
   2. Vote on changing right to vote prerequisites
10. Miscellaneous

### Vote details for agenda point 8:

Resolution: "The AGM decides that future AGMs can be called as virtual meetings where the members taking part must execute their other membership rights via means of electronic communication without being present at the meeting location."

Rationale: The board would like to have the option of doing future general assemblies virtually to keep our options open in case of technical, logistical or other issues. Approving this resolution will put us on firm legal ground in case we have to or want to do a fully virtual general assembly in the future.

### Vote details for agenda point 9:

- **9.1** The [rules of procedure section $2.3](https://ev.kde.org/rules/online_voting/#23-start-of-voting) states that voting starts by sending ballots:
  > When the discussion period has finished the voting is started by sending ballots to all active members. The ballots should include the text of the proposal which is voted about.

  The text is to be amended to indicate that the ballot notification is sent to the membership list:

  > When the discussion period has finished the voting is
  >     started by sending a ballot notification to the KDE
  >     e.V. membership mailing list. It is each member's responsibility
  >     to keep a current email address subscribed to the list.
  >
  > The ballots should include the text of the proposal
        which is voted about.
- **9.2** The [rules of procedure section $4](https://ev.kde.org/rules/online_voting/#4-right-to-vote) states prerequisites to voting:
  > All members of the KDE e.V. which are active members at the time the voting period is started have the right to cast a vote. The prerequisite for taking part in online votes is a valid email address in the member database. The member is solely responsible for initially setting and updating his email address in the member database.

  The text is to be amended to indicate that the notification happens through the e.V. membership mailing list.
  The prerequisite is dropped, because the notifications are no longer sent individually.

  > All members of KDE e.V. who are active members at
  >     the time the voting period is started have the right
  >     to cast a vote. Vote notifications are sent to the KDE e.V.
  >     membership mailing list. Members are personally
  >     responsible for maintaining their email address for
  >     the mailing list and in KDE's identity management system.
