---
title: "KDE e.V. Homepage"
---

<img src="images/ev_large.png" class="img-fluid" style="float: right; width: 395px; height: auto;" alt="KDE e.V. logo" />

KDE e.V. is a registered non-profit organization that represents
the <a href="https://kde.org">KDE Community</a> in legal and financial matters.
Read our <a href="/whatiskdeev/">mission statement</a>.

KDE e.V. is made up of its [members](/members/), from which the
[board of KDE e.V.](/corporate/board/) is elected.  KDE e.V. has
[supporting members](/supporting-members/) who provide the material
support to carry out KDE e.V.'s activities.

On this site you will find information about [getting involved](/getinvolved/),
reports about [past](/reports/) and [ongoing](/activities) activities, and
official documents like the [articles of association](/corporate/statutes/),
additional [rules and policies](/rules/), and [forms](/resources/) for official
activities.
