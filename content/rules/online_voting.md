---
title: "Rules of Procedures for Online Voting"
layout: page
---

The articles of association of the KDE e.V. provide that certain decisions can
be taken by online voting. This includes voting about new members and
deciding about certain rules of procedures. In addition the KDE e.V. allows
to use online voting for decisions which aren't covered in the articles of
association. There are several different types of votes which slightly differ
in procedure and method to calculate results. These rules regulate the
procedure of online votes and how results are calculated. The intention of
these rules is to provide a fair and pragmatic way to let the KDE e.V. take
decisions by online voting.

## 1 Voting Administrators

The board of the KDE e.V. assigns one or more voting administrators which are
responsible for technically performing the voting. This includes sending out
the ballots, receiving the votes, calculating and publishing the results.

The voting administrators must not disclose any information about the identity
of the voters or any other information which isn't contained in the published
voting results.

## 2 General procedure for online voting

An online voting is initiated by a voting proposal followed by a discussion
period. After the discussion period the voting period is started. The voting
is finished by calculating and publishing the voting results.

Results of online votes are effective immediately following publication of the
results unless otherwise stated in the voting proposal.

### 2.1 Voting Proposal

An online voting is initiated by a proposal for voting which states the
subject of the voting. The proposal is sent by email to the membership
mailing list and has to include exact and complete information what is voted
upon. Only the information which is directly included in the mail sent as
voting proposal is subject of the voting.

The voting proposal email has to explicitly be marked as voting proposal by
starting the email subject line with one of the following strings, which are
**not** case-sensitive:
 - *Voting Proposal:*
 - *Vote Proposal:*
 - *Member Proposal:*

Any active member can initiate a voting by sending a voting proposal.

The member who has initiated a vote can retract the voting proposal by sending
an email to the membership list stating the intention to retract the
proposal.

A proposal can only be retracted within the discussion period.

### 2.2 Discussion Period

The discussion period begins on the date the membership mailing list receives
the voting proposal. Unless specified otherwise in the procedures for the
specific type of vote (see: Types of Votes), the discussion period lasts for
two weeks. The discussion period should be used to discuss the voting and
form opinions about the options which are available for voting.

### 2.3 Start of Voting

When the discussion period has finished, the voting is started by sending a ballot notification to the KDE e.V. membership mailing list.
It is each member's responsibility to keep a current email address subscribed to the list.

The ballots should include the text of the proposal which is voted about.

### 2.4 Voting Period

The voting period is started by sending out the ballots. Unless specified
otherwise in the procedures for the  specific type of vote (see: Types of
Votes), the voting period lasts for two weeks.
During the voting period the active members cast their votes. Only
votes cast within the voting period are considered for the results of the
voting.

### 2.5 End of Voting

After the voting period all cast votes are counted and the results of the
voting are published.

The results of a vote are published to the KDE e.V. membership by sending them
to the membership mailing list. They have to include the number of persons
permitted to vote, the total number of votes, and the number of votes for
each available voting option. The results should also include a statement, if
the voting was valid according to section 5.

## 3 Types of Votes

There are three types of votes:
Votes about new members (3.1),
elections of groups of people (3.2),
votes for decision (3.3).

### 3.1 Voting about new members

The discussion period is started by sending a new member proposal to the
membership mailing list. Within the discussion period at least two other members
have to declare their support for the vote, otherwise the proposal is considered
to be rejected. If two members declare their support the voting is started after
the discussion period.

The discussion period has a duration of one week and is extended to two
weeks, if requested by a member.

The voting period has a duration of one week.

For new member votes there are three options: "Yes", "No", "Abstention". The
new member is accepted, if there are more "Yes" than "No" votes and the vote
isn't invalid according to section 5.

### 3.2 Electing groups of persons

For elections of groups of people there is an additional candidacy period
before the voting proposal is sent. The person responsible for the execution
of the voting sends a call for candidates to the membership mailing list
which starts the candidacy period. If there is no explicit rule about who is
responsible for execution of the voting, the board of the e.V. is
responsible.

The candidacy period lasts two weeks. All members who declare their candidacy
in a statement sent to the membership list become candidates for the
election.

After the candidacy period the list of candidates is sent as voting proposal.
This starts the two-weeks discussion period.

The candidates are voted on as options according to section 3.4.

### 3.3 Voting about decisions

The KDE e.V. can officially take decisions or decide about official statements
on request of at least three active member. One member has to send the voting
proposal to the membership mailing list. This starts the discussion period.
Within the discussion period at least two other members have to declare their
support for the vote, otherwise the proposal is considered to be rejected. If
two members declare their support the voting is started after the discussion
period.

The responsible party for execution of the voting sends the voting proposal
which includes all options. This starts the discussion period.

After the discussion period the voting is started. The options are voted on
according to section 3.4.


### 3.4 Voting System

#### 3.4.1 Multiple options

If there are more than two options (excluding abstention), the following voting
system shall be used:

The members are asked to rank the options in terms of their preference into a
ballot. A ballot may contain two options of the same rank. All options must be
ranked. Each member may make up to one ballot.

<!-- Leave this in HTML-style list, because the text refers to specific
     steps using the name generated by HTML.
-->

The winners are found by:
<ol style="list-style-type:lower-roman;">
  <li>
    Given two options A and B, V(A,B) is the number of voters who prefer
    option A over option B.
  </li>
  <li>
    From the list of options, we generate a list of pairwise defeats.
    <ul>
      <li>
        An option A defeats an option B, if V(A,B) is strictly greater than
        V(B,A).
      </li>
    </ul>
  </li>
  <li>
    From the list of pairwise defeats, we generate a set of transitive defeats.
    <ul>
      <li>
        An option A transitively defeats an option C if A defeats C or if there
        is some other option B where A defeats B AND B transitively defeats C.
      </li>
    </ul>
  </li>
  <li>
    We construct the Schwartz set from the set of transitive defeats.
    <ul>
      <li>
        An option A is in the Schwartz set if for all options B, either A
        transitively defeats B, or B does not transitively defeat A.
      </li>
    </ul>
  </li>
  <li>
    If there are defeats between options in the Schwartz set, we drop the
    weakest such defeats from the list of pairwise defeats, and return to step
    (iii).
    <ul>
      <li>
        A defeat (A,X) is weaker than a defeat (B,Y) if V(A,X) is less than
        V(B,Y). Also, (A,X) is weaker than (B,Y) if V(A,X) is equal to V(B,Y)
        and V(X,A) is greater than V(Y,B).
      </li>
      <li>
        A weakest defeat is a defeat that has no other defeat weaker than it.
        There may be more than one such defeat.
      </li>
    </ul>
  </li>
  <li>
    If the number of options in the Schwartz set does not exceed the number of
    required winners, then the winners are the Schwartz set. If the number of
    members of the Schwartz set exceeds the number of required winners, the
    board chooses which of the members of the Schwartz set wins.
  </li>
  <li>
    If this results in an insufficient number of winners, then the candidates
    which have already won according to step (vi) should be removed from the
    list of options, and then this procedure is repeated from step (i).
  </li>
</ol>

> Parts of this section are copied from the Constitution for the Debian
> Project (v1.4), [https://www.debian.org/devel/constitution](https://www.debian.org/devel/constitution)

#### 3.4.2 Two options

If the number of options is exactly two (excluding abstension), then:
-    Each active member can vote with "Yes", "No" or "Abstain".
-   If there are more "Yes" than "No" votes and the vote isn't invalid according
    to section 5, the decision is accepted.


## 4 Right to vote

All members of KDE e.V. who are active members at the time the voting period is started have the right to cast a vote.
Vote notifications are sent to the KDE e.V. membership mailing list.
Members are personally responsible for maintaining their email address for the mailing list and in KDE's identity management system.

## 5 Quorum

The result of a voting only becomes effective when the number of votes which
have chosen an option different from "Abstention" is greater than 20 percent
of the number of active members. If this quorum isn't reached the voting is
considered invalid. An invalid voting can be repeated, but not before a
period of four weeks has passed between publishing the results of the invalid
vote and the proposal for the repeated voting.

## 6 Anonymity

Online votes are anonymous. The information which members participated in the
vote and for which option they voted aren't disclosed. The voting
administrators are allowed to track this information for technically executing
the vote. The information mustn't be used for other purposes and mustn't be made
available to any other people.

## 7 Publication of voting results

All voting results except for rejected votes about new members are published on
the KDE e.V. web site, excluding the exact numbers of votes.

## 8 Modification of voting procedure

The board may modify the voting procedures for individual votes upon request
of the e.V. membership if good reason for the change, as determined by the
good judgment of the board, is provided.

<hr/>

### History

 - The rules of procedure for online voting have been decided by the
   membership of the KDE e.V. at the general assembly 2006 in Dublin on September
   25th 2006.
 - At the general assembly 2008 in Sint-Katelijne-Waver on August 11th
   2008 the membership decided to introduce the Condorcet method for votings about
   decisions with more than two options.
 - An online vote concluded June 19th 2022 extended section 2.1.
 - At the general assembly 2023 online and at the Berlin office on September 9th 2023
   the membership amended section 2.3 to indicate where the ballots are sent
   and admended section 4 to be consistent with section 2.3.
