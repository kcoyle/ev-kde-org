---
title: "KDE e.V. Travel Cost Reimbursement Policy"
layout: page
---

## Eligibility

+ KDE e.V. can only reimburse costs that incurred in pursuit of
  the goals of KDE e.V. or for KDE e.V. running its business.</p>
  
  The following reasons are usually valid:
  + Attending the general assembly of the KDE e.V. as a member
  + Giving a talk at a KDE event
  + Representing KDE at a conference or an important meeting
  + Taking part in a developer sprint or other meeting as a KDE contributor
+ You should first try to get your employer to pay for your trip, find a third
  party sponsor or consider paying for it from your own funds.
+ If you wouldn't be able to attend the event because of the costs or paying for
  it yourself would be a great burden for you because you have no or a low
  income or the costs are especially high, you should request a
  reimbursement.


## Amounts

KDE e.V. usually reimburses up to 100% of the transportation and lodging
expenses for the cheapest reasonable way, subject to availability of funds.
Food expenses are not covered.

## Travel Support Workflow

You cannot get the money beforehand, you need to pay by yourself and then get your expenses reimbursed afterwards. If you are absolutely unable to do that, it is possible for the e.V. to directly pay your expenses.

1. The board must approve the budget <strong>before</strong> the trip.
   + If it's about attending a <em>KDE Sprint</em>, it will be managed by the sprint organization.
   + If you want to be funded to attend an <em>external event</em>, please contact the KDE e.V. Board &lt;kde-ev-board@kde.org&gt;
     to make sure it can be included in the budget.

     Please provide the KDE e.V. board with the following information:
     + Who you are and where in KDE you are involved
     + What's the purpose of your trip
     + Why it is a good thing to sponsor you
     + Why you need a subsidy
     + What are your costs and how much should your subsidy be
1. Once approved, you must create a new <em>Travel Support Request</em> in <a href="https://reimbursements.kde.org/" target="_blank">https://reimbursements.kde.org</a>.
1. Once the <em>Travel Support Request</em> is approved, accept it if everything is in order.
1. Attend and enjoy your meeting! Remember to collect the receipts.
1. After the travel, you must create a new <em>Reimbursement</em> in <a href="https://reimbursements.kde.org/" target="_blank">https://reimbursements.kde.org</a> less than 3 months after the event and add all receipts (scanned copies) and additional documents. <em>Reimbursements</em> without proper receipts won't be processed.
1. As part of the reimbursement mentioned about you must\[\*\] link to a dot story or a post on Planet KDE, explaining how it helped KDE and what you have learned and experienced in relation to KDE during your trip, this way the wider KDE Community can learn about the experience too. (* there are some exceptions)
1. Then the <em>Reimbursement</em> will be processed, and paid.

## Additional Information

Generally, reimbursements will be transfered to bank or PayPal accounts.

+ For transfers to bank accounts, the following information is needed:
  + For IBAN transfers: name of the account holder, name of the financial institution, IBAN number.
  + For other transfers: name of the account holder, name of the financial institution, account number, national bank code, postal address
        of the bank and the country.
+ For transfers to paypal accounts, we need the paypal id.

<hr/>

<em>This policy is maintained by the board of the KDE e.V.</em>
