---
title: "KDE e.V. Fundraising Working Group"
layout: page
---

### Goals

The Fundraising Working Group supports the Board in ensuring the financial sustainability of KDE by keeping good relations with our existing supporters and recruiting new ones.

The Fundraising Working Group has the following tasks:

+ Identify fundraising opportunities and coordinate the execution of fundraising campaigns.
+ Maintain and improve the technical infrastructure used in fundraising campaigns.
+ Coordinate the production and outreach of promotional material for fundraising campaigns.

### Rules

> The Fundraising Working Group has no specific rules.

### Members

The current members of the KDE e.V. Fundraising Working Group are:

+ Lays Rodrigues
+ Carl Schwan
+ Niccolò Venerandi

### Contact 

You can contact the Fundraising Working Group under [kde-ev-campaign@kde.org](mailto:kde-ev-campaign@kde.org).

We are also active on IRC and Matrix.

 * IRC: #kde-ev-fundraising on irc.libera.chat.
 * Matrix: https://go.kde.org/matrix/#/#kde-ev-fundraising:kde.org
