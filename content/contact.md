---
title: Contact
menu:
  main:
    parent: contact
    name: Contact Us
    weight: 1
---

## Email

<p>Contact the <a href="/corporate/board">board</a> of the KDE e.V. by
sending email to <a href="mail&#x74;&#x6f;:k&#x64;&#x65;-&#101;&#x76;&#0045;&#0098;o&#97;&#114;d&#00064;&#x6b;d&#x65;.&#111;&#114;&#103;">&#107;d&#101;&#x2d;e&#x76;&#x2d;bo&#x61;&#00114;d&#00064;&#107;&#100;&#101;.&#111;&#x72;g</a>.
That's the preferred way of contacting the KDE e.V.
</p>

The <a href="/workinggroups/">KDE e.V. working groups</a> have separate email
addresses. For issues that are related to the specific
mandates of the working groups, these addresses
are to be preferred over the general KDE e.V. board address.

<ul>
<li>Promotion and Communication Working Group: <a href='ma&#105;&#108;to&#58;&#112;res%73&#64;&#37;6Bde&#46;o&#114;&#103;'>pre&#115;&#115;&#64;kde&#46;&#111;r&#103;</a></li>
<li>Sysadmin Working Group: <a href="&#109;a&#105;l&#x74;o:&#x73;&#x79;sadmi&#x6e;&#0064;k&#x64;&#00101;&#00046;&#00111;rg">sy&#115;a&#100;&#x6d;i&#x6e;&#x40;&#x6b;d&#x65;.o&#114;g</a></li>
<li>Community Working Group: <a href="m&#x61;&#x69;&#x6c;&#00116;&#00111;:co&#x6d;&#109;unit&#x79;-wg&#x40;&#x6b;d&#x65;.o&#114;g">co&#109;&#x6d;unit&#x79;-wg&#x40;&#x6b;d&#x65;.o&#114;g</a></li>
</ul>


## Postal Address

The official postal address of the KDE e.V. is:

```
KDE e.V.
Prinzenstraße 85 F
10969 Berlin
Germany
```

## Telephone

```
+49 30-2023 7305-0
```

You can reach the KDE e.V. office personally by phone during regular German
office hours.

## Fax

```
+49 30-2023 7305-9
```

<h2><a name="bank" />Bank Account</h2>

```
K Desktop Environment e.V.
Account-Nr. 0666446
BLZ 20070024
Deutsche Bank Privat und Geschaeftskunden
Hamburg, Germany

IBAN     : DE82200700240066644600
SWIFT-BIC: DEUTDEDBHAM
```

<h2>Impressum</h2>

<p>See the <a href="https://kde.org/community/whatiskde/impressum">Impressum</a> (<a
href="https://kde.org/community/whatiskde/impressum-en">English version</a>) for this
web site
which is required by German law for some more detailed contact information.</p>
