---

title: "2023 KDE e.V. Report"

layout: report

description: "KDE e.V.'s latest report is out. In this issue, we cover the
activities of our association in 2023. KDE's yearly report gives a comprehensive
overview of all that has happened during 2022, covering the progress we have
made with KDE's Plasma and applications as well as results from community
sprints, conferences, and external events the KDE community has participated in
worldwide."

date: 2024-06-30
year: 2023
issue_number: 40

parts:
    - title: Home
      anchor: home
      home: true
    - title: Welcome
      heading: Welcome to KDE's Annual Report 2023
      image: images/logos/kde-logo.png
      author: Aleix Pol
      include: 00_Welcome.md
    - title: Featured Article
      heading: Featured article - The year we did this other thing too
      image: images/logos/kde-logo.png
      author: Paul Brown and Nate Graham
      include: 00_Featured.md
    - title: Supported Activities
      items:

# Events ordered according to the date they were held
        - title: Developer Sprints, conferences and Activities
          heading: Supported Activities ‒ Developer Sprints and Conferences
          items:
           - include: 01_PIM_Sprint.md
             author: From the notes of Kevin Ottens and the PIM team
             heading: PIM Sprint
             image: images/logos/kde-pim-logo.png
           - include: 01_Plasma_Sprint.md
             author: From the notes of Kai Uwe, Nate Graham and Volker Krause
             heading: Plasma Sprint
             image: images/logos/plasma.png
           - include: 01_Board_Sprint.md
             author: By Aniqa Khokhar
             heading: KDE e.V. Board Sprint
             image: images/logos/ev_large.png
           - include: 01_Akademy_Pre-event.md
             author: By Aniqa Khokhar
             heading: "Akademy Pre-event: Making a Difference"
             image: images/logos/akademy_logo.png
           - include: 01_Akademy-es.md
             author: By Aniqa Khokhar
             heading: Akademy-es
             image: images/logos/akademy_logo.png
           - include: 01_25_years_KFQF.md
             author: By Aniqa Khokhar
             heading: 25 Years of the KDE Free Qt Foundation
             image: images/logos/kde-logo.png
           - include: 01_Akademy.md
             author: From the notes by Aniqa Khokhar and Paul Brown
             heading: Akademy
             image: images/logos/akademy_logo.png
           - include: 01_Promo_Sprint.md
             author: By Aniqa Khokhar
             heading: Promo Sprint
             image: images/logos/promo_konqi.png
           - include: 01_Kdenlive_Sprint.md
             author: By Jean-Baptiste Mardelle
             heading: Kdenlive Sprint
             image: images/logos/kdenlive-logo.png

## Items ordered alphabetically
        - title: Projects
          heading: Projects and Apps
          items:
           - include: 02_Arianna.md
             author: By Carl Schwan
             heading: Arianna - Manage and read your ebooks
             image: images/logos/arianna.png
           - include: 02_Francis.md
             author: By Carl Schwan
             heading: Francis - Manage your work time
             image: images/logos/francis.png
           - include: 02_GSoC.md
             author: By GSOC Contributors
             heading: Google Summer of Code
             image: images/logos/gsoc.png
           - include: 02_KDEEco_Handbook.md
             author: By KDE Eco Team
             heading: KDE Eco Handbook
             image: images/logos/KDE-eco-logo.png
           - include: 02_SoK.md
             author: By By Johnny Jazeix and Joseph P. De Veaugh-Geiss
             heading: Season of KDE
             image: images/logos/mentor_konqi.png
        
#        - title: KDE-Powered Products
#          heading: Products
#          items:
#           - include: 03_Slimbook_4.md
#             author: By Paul Brown
#             heading: KDE Slimbook 4
#             image: images/logos/slimbook.png
    
## Events ordered according to the date they were held
        - title: Trade Shows and Community Events
          heading: Trade Shows and Community Events
          items:
           - include: 04_FOSDEM.md
             heading: FOSDEM
             author: By Aniqa Khokhar
             image: images/logos/FOSDEMx47.png
           - include: 04_LAS.md
             heading: Linux App Summit
             author: By Aniqa Khokhar
             image: images/logos/LAS.png
           - include: 04_Other_Events.md
             heading: Other Events
             author: By Aniqa Khokhar
             image: images/logos/kde-logo.png

    - title: Reports
      items:
        - title: Working Groups
          heading: Working Groups
          items:
           - heading: Sysadmin
             include: 06_Sysadmin.md
             image: images/logos/kde-logo.png
             author: By Ben Cooksley
           - heading: Financial Working Group
             include: 06_FIWG.md
             image: images/logos/kde-logo.png
             author: By Eike Hein, Marta Rybczynska and Till Adam
           - heading: Fundraising
             include: 06_Fundraising.md
             image: images/logos/kde-logo.png
             author: By Aniqa Khokhar

    - title: Community
      items:
        - title: Community News
          heading: Community News
          items:
          - heading: Highlights
            include: 07_highlights.md
            author: By Paul Brown
          - heading: New Members
            include: 07_new-members.md
            author: ""

    - title: Partners
      items:
        - title: Partners and Sponsors
          heading: Partners and Sponsors
          items:
          - heading: g10 Code Becomes a KDE Patron
            include: 08_g10_Code.md
            author: ""
            image: images/logos/GnuPG-Logo.png
          - heading: Kubuntu Focus Becomes a KDE Patron
            include: 08_Kubuntu_Focus.md
            author: ""
            image: images/logos/kfocus.png
          - heading: Thoughts from Partners
            include: 08_quotes.html
            author: ""
          - heading: Partners
            include: 08_partners.md
            author: ""

    - title: KDE e.V. Board of Directors
      heading: KDE e.V. Board of Directors
      include: 09_Board.html
      author: ""
      nonav: true
---

<section id="about-kdeev" class="bg-light">
    <div class="container">
        <h2>About KDE e.V.</h2>
        <p>
            <a href="https://ev.kde.org/" target="_blank">KDE e.V.</a> is a
registered non-profit organization that represents the <a
href="http://www.kde.org" target="_blank">KDE Community</a> in legal and
financial matters. The KDE e.V.'s purpose is the promotion and distribution of
free desktop software in terms of free software, and the program package "K
Desktop Environment (KDE)" in particular, to promote the free exchange of
knowledge and equality of opportunity in accessing software as well as
education, science and research.
        </p>
        <ul class="address hidden">
            <li><i class="fa fa-map-marker"></i><span> Address:</span> Prinzenstraße 85 F,10969 Berlin, Germany </li>
            <li><i class="fa fa-phone"></i> <span> Phone:</span> +49 30 202373050 </li>
            <li><i class="fa fa-envelope"></i> <span> Email:</span><a href="mailto:kde-ev-board@kde.org"> kde-ev-board@kde.org</a></li>
            <li><i class="fa fa-globe"></i> <span> Website:</span> <a href="#">ev.kde.org</a></li>
        </ul>
        <p>
            Report prepared by Aniqa Khokhar and Paul Brown, with help and
sections written by Aleix Pol, Joseph P. De Veaugh-Geiss, Nate Graham,
Jean-Baptiste Mardelle, Kai Uwe, Volker Krause, Kevin Ottens, Ben Cooksley,
Johnny Jazeix, Carl Schwan, the KDE Eco Team, the GSOC Contributors, the Plasma
Team, and the Promo Team at large.
        </p>        
        <p>
            This report is published by KDE e.V., copyright 2024, and licensed
under <a href="https://creativecommons.org/licenses/by/3.0/">Creative
Commons-BY-3.0</a>.
        </p>
        <a href="https://ev.kde.org/" target="_blank" class="text-center"> <img class="img-responsive" src="/reports/images/images/logo.png" alt=""/> </a>
    </div>
</section>
