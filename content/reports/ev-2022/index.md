---

title: "2022 KDE e.V. Report"

layout: report

description: "KDE e.V.'s latest report is out. In this issue, we cover the activities of our association in 2022. KDE's yearly report gives a comprehensive overview of all that has happened during 2022, covering the progress we have made with KDE's Plasma and applications as well as results from community sprints, conferences, and external events the KDE community has participated in worldwide."

date: 2023-07-10
year: 2022
issue_number: 39

parts:
    - title: Home
      anchor: home
      home: true
    - title: Welcome
      heading: Welcome to KDE's Annual Report 2022
      image: images/logos/kde-logo.png
      author: Aleix Pol
      include: 00_Welcome.md
    - title: Featured Article
      heading: Featured article - KDE's Goals of 2022 and Beyond
      image: images/logos/kde-logo.png
      author: Paul Brown
      include: 00_Featured.md
    - title: Supported Activities
      items:

# Events ordered according to the date they were held
        - title: Developer Sprints, conferences and Activities
          heading: Supported Activities ‒ Developer Sprints and Conferences
          items:
           - include: 01_Plasma_sprint.md
             author: By Volker Krause
             heading: Plasma Sprint
             image: images/logos/plasma.png
           - include: 01_SoK.md
             author: By Johnny Jazeix
             heading: Season of KDE
             image: images/logos/kde-logo.png
           - include: 01_Eco_sprints.md
             author: From the Notes by Joseph P. De Veaugh-Geiss
             heading: KDE Eco Sprints
             image: images/logos/KDE-eco-logo.png
           - include: 01_Board_sprints.md
             author: By Aniqa Khokhar
             heading: KDE e.V. Board Sprints
             image: images/logos/kde-logo.png
           - include: 01_GSoC.md
             author: By Johnny Jazeix
             heading: Google Summer of Code
             image: images/logos/gsoc.png
           - include: 01_Promo_sprint.md
             author: From the notes of Carl Schwan and Joseph P. De Veaugh-Geiss
             heading: Promo Sprint
             image: images/logos/Mascot_konqi-commu-journalist_small.png
           - include: 01_Akademy-es.md
             author: By Aniqa Khokhar
             heading: Akademy-es
             image: images/logos/akademy_logo.png
           - include: 01_Akademy.md
             author: From the notes by Aniqa Khokhar, Jonathan Esk-Riddell, and Paul Brown
             heading: Akademy
             image: images/logos/akademy_logo.png

## Items ordered alphabetically
        - title: Projects
          heading: Projects & Apps
          items:
           - include: 02_Ghostwriter.md
             author: By the Ghostwriter team
             heading: Ghostwriter - A distraction-free writing experience
             image: images/logos/ghostwriter.png
           - include: 02_Kodaskanna.md
             author: By the Kodaskanna team
             heading: Kodaskanna - Read data from barcodes
             image: images/logos/kodaskanna.png
        
        - title: KDE-Powered Products
          heading: Products
          items:
           - include: 03_Slimbook_4.md
             author: By Paul Brown
             heading: KDE Slimbook 4
             image: images/logos/slimbook.png
    
## Events ordered according to the date they were held
        - title: Trade Shows and Community Events
          heading: Trade Shows and Community Events
          items:
           - include: 04_FOSDEM.md
             heading: FOSDEM
             author: By Aniqa Khokhar
             image: images/logos/FOSDEMx47.png
           - include: 04_LAS.md
             heading: Linux App Summit
             author: By Aniqa Khokhar
             image: images/logos/LAS.png
           - include: 04_QtdevCon.md
             heading: Qt DevCon
             author: By Aniqa Khokhar
             image: images/logos/qt.png
           - include: 04_SCaLE_19x.md
             heading: SCaLE 19x
             author: By Bhavisha Dhruve
             image: images/logos/scale.png
           - include: 04_AAHSFF.md
             heading: All-American High School Film Festival
             author: By Simon Redman
             image: images/logos/AAHSFF.png
           - include: 04_Latinoware.md
             heading: Latinoware
             author: By Blumen Herzenschein
             image: images/logos/latinoware.png
           - include: 04_UbuntuSummit.md
             heading: Ubuntu Summit
             author: By Jonathan Esk-Riddell
             image: images/logos/Ubuntu.png
           - include: 04_Qt_World_Summit.md
             heading: Qt World Summit
             author: By Aniqa Khokhar
             image: images/logos/qt.png
           - include: 04_QtCon.md
             heading: QtCon Brazil
             author: By Aniqa Khokhar
             image: images/logos/qt.png
           - include: 04_OpenUK.md
             heading: OpenUK Awards
             author: By Jonathan Esk-Riddell
             image: images/logos/openuk.png

    - title: Reports
      items:
        - title: Working Groups
          heading: Working Groups
          items:
           - heading: Sysadmin
             include: 06_Sysadmin.md
             image: images/logos/kde-logo.png
             author: By Ben Cooksley
           - heading: Financial Working Group
             include: 06_FIWG.md
             image: images/logos/kde-logo.png
             author: By Eike Hein, Marta Rybczynska and Till Adam
           - heading: Fundraising
             include: 06_Fundraising.md
             image: images/logos/kde-logo.png
             author: By Aniqa Khokhar

    - title: Community Highlights
      heading: Highlights
      include: 07_highlights.md
      author: Nate Graham
    - title: Thoughts from Partners
      heading: Thoughts from Partners
      include: 07_quotes.html
      author: ""
      nonav: true
    - title: New Members
      heading: New Members
      include: 07_new-members.md
      author: ""
      nonav: true
    - title: KDE e.V. Board of Directors
      heading: KDE e.V. Board of Directors
      include: 07_Board.html
      author: ""
      nonav: true
    - title: Partners
      heading: Partners
      include: 07_partners.md
      author: ""
      nonav: true
---

<section id="about-kdeev" class="bg-light">
    <div class="container">
        <h2>About KDE e.V.</h2>
        <p>
            <a href="https://ev.kde.org/" target="_blank">KDE e.V.</a> is a registered non-profit organization that represents the <a href="http://www.kde.org" target="_blank">KDE Community</a> in legal and financial matters. The KDE e.V.'s purpose is the promotion and distribution of free desktop software in terms of free software, and the program package "K Desktop Environment (KDE)" in particular, to promote the free exchange of knowledge and equality of opportunity in accessing software as well as education, science and research.
        </p>
        <ul class="address hidden">
            <li><i class="fa fa-map-marker"></i><span> Address:</span> Prinzenstraße 85 F,10969 Berlin, Germany </li>
            <li><i class="fa fa-phone"></i> <span> Phone:</span> +49 30 202373050 </li>
            <li><i class="fa fa-envelope"></i> <span> Email:</span><a href="mailto:kde-ev-board@kde.org"> kde-ev-board@kde.org</a></li>
            <li><i class="fa fa-globe"></i> <span> Website:</span> <a href="#">ev.kde.org</a></li>
        </ul>
        <p>
            Report prepared by Aniqa Khokhar and Paul Brown, with help and sections written by Aleix Pol, Jonathan Esk-Riddell, Joseph P. De Veaugh-Geiss, Nate Graham, Plasma Team, the Plasma Mobile Team and the Promo Team at large.
        </p>        
        <p>
            This report is published by KDE e.V., copyright 2023, and licensed under <a href="https://creativecommons.org/licenses/by/3.0/">Creative Commons-BY-3.0</a>.
        </p>
        <a href="https://ev.kde.org/" target="_blank" class="text-center"> <img class="img-responsive" src="/reports/images/images/logo.png" alt=""/> </a>
    </div>
</section>
