---
title: Reports
menu:
  main:
    parent: info
    weight: 3
nosubpage: true
---

The KDE e.V. collects reports about its activities. Here you can find all
the public [community reports](#kde-ev-community-reports) and [minutes of the AGM](#meetings).

## KDE e.V. Community Reports

> The community reports (annual or quarterly) are digests of what has happened
> in the KDE community, supported by KDE e.V. The 2014 and earlier reports
> (the quarterly reports) are PDF files.

### 2022

- <a href="ev-2022/" target="_blank">KDE e.V. Report for 2022 (Issue 39)</a>

### 2021

- <a href="ev-2021/" target="_blank">KDE e.V. Report for 2021 (Issue 38)</a>

### 2020

- <a href="ev-2020/" target="_blank">KDE e.V. Report for 2020 (Issue 37)</a>


### 2019

- <a href="ev-2019/" target="_blank">KDE e.V. Report for 2019 (Issue 36)</a>


### 2018

- <a href="ev-2018/" target="_blank">KDE e.V. Report for 2018 (Issue 35)</a>


### 2017

- <a href="ev-2017/" target="_blank">KDE e.V. Report for 2017 (Issue 34)</a>


### 2016

- <a href="ev-2016/" target="_blank">KDE e.V. Report for 2016 (Issue 33)</a>


### 2015

- <a href="ev-2015H2/" target="_blank">KDE e.V. Report for 2nd Half of 2015 (Issue 32)</a>
- <a href="ev-2015H1/" target="_blank">KDE e.V. Report for 1st Half of 2015 (Issue 31)</a>


### 2014

- [KDE e.V. Quarterly Report 2014Q4 (Issue 30)](ev-quarterly-2014_Q4.pdf)
- [KDE e.V. Quarterly Report 2014Q3 (Issue 29)](ev-quarterly-2014_Q3.pdf)
- [KDE e.V. Quarterly Report 2014Q1Q2 (Issue 28)](ev-quarterly-2014_Q1Q2.pdf)


### 2013

- [KDE e.V. Quarterly Report 2013Q4 (Issue 27)](ev-quarterly-2013_Q4.pdf)
- [KDE e.V. Quarterly Report 2013Q2/Q3 (Issue 26)](ev-quarterly-2013_Q2Q3.pdf)
- [KDE e.V. Quarterly Report 2013Q1 (Issue 25)](ev-quarterly-2013_Q1.pdf)


### 2012

- [KDE e.V. Quarterly Report 2012Q4 (Issue 24)](ev-quarterly-2012_Q4.pdf)
- [KDE e.V. Quarterly Report 2012Q3 (Issue 23)](ev-quarterly-2012_Q3.pdf)
- [KDE e.V. Quarterly Report 2012Q2 (Issue 22)](ev-quarterly-2012_Q2.pdf)
- [KDE e.V. Quarterly Report 2012Q1 (Issue 21)](ev-quarterly-2012_Q1.pdf)


### 2011

- [KDE e.V. Quarterly Report 2011Q4 (Issue 20)](ev-quarterly-2011_Q4.pdf)
- [KDE e.V. Quarterly Report 2011Q3 (Issue 19)](ev-quarterly-2011_Q3.pdf)
- [KDE e.V. Quarterly Report 2011Q2 (Issue 18)](ev-quarterly-2011_Q2.pdf)
- [KDE e.V. Quarterly Report 2011Q1 (Issue 17)](ev-quarterly-2011_Q1.pdf)


### 2010

- [KDE e.V. Quarterly Report 2010Q4 (Issue 16)](ev-quarterly-2010Q4.pdf)
- [KDE e.V. Quarterly Report 2010Q3 (Issue 15)](ev-quarterly-2010Q3.pdf)
- [KDE e.V. Quarterly Report 2010Q2 (Issue 14)](ev-quarterly-2010Q2.pdf)
- [KDE e.V. Quarterly Report 2009Q2-2010Q1 (Issue 13)](ev-quarterly-2009Q2-2010Q1.pdf)


### 2009

- [KDE e.V. Quarterly Report 2009Q1 (Issue 12)](ev-quarterly-2009Q1.pdf)


### 2008

- [KDE e.V. Quarterly Report 2008Q3/Q4 (Issue 11)](ev-quarterly-2008Q3-Q4.pdf)
- [KDE e.V. Quarterly Report 2008Q1/Q2 (Issue 10)](ev-quarterly-2008Q1-Q2.pdf)


### 2007

- [KDE e.V. Quarterly Report 2007Q3/Q4 (Issue 9)](ev-quarterly-2007Q3-Q4.pdf)
- [KDE e.V. Quarterly Report 2007Q2 (Issue 8)](ev-quarterly-2007Q2.pdf)
- [KDE e.V. Quarterly Report 2007Q1 (Issue 7)](ev-quarterly-2007Q1.pdf)


### 2006

- [KDE e.V. Quarterly Report 2006Q4 (Issue 6)](ev-quarterly-2006Q4.pdf)
- [KDE e.V. Quarterly Report 2006Q3 (Issue 5)](ev-quarterly-2006Q3.pdf)
- [KDE e.V. Quarterly Report 2006Q2 (Issue 4)](ev-quarterly-2006Q2.pdf)
- [KDE e.V. Quarterly Report 2006Q1 (Issue 3)](ev-quarterly-2006Q1.pdf)


### 2005

- [KDE e.V. Quarterly Report 2005Q4 (Issue 2)](ev-quarterly-2005Q4.pdf)
- [KDE e.V. Quarterly Report 2005Q3 (Issue 1)](ev-quarterly-2005Q3.pdf)


## Meetings

> The KDE e.V. holds a general assembly of its members each year.
> At the assembly, reports about the activity of the e.V. are given,
> elections are held and decisions about matters of the e.V. are taken.

- 2023 AGM minutes ([german](2023-de))<!-- ([english](2023-en)) -->
- 2022 AGM minutes ([german](2022-de))([english](2022-en))
- 2021 AGM minutes ([german](2021-de))([english](2021-en))
- 2020 AGM minutes ([german](2020-de))([english](2020-en))
- 2019 AGM minutes ([german PDF](2019-de.pdf))
- 2018 AGM minutes ([german PDF](2018-de.pdf))
- 2017 AGM minutes ([german PDF](2017-de.pdf))([english PDF](2017-en.pdf))
- 2016 AGM minutes ([german PDF](2016-de.pdf))
- 2015 AGM minutes ([german PDF](2015-de.pdf))([english PDF](2015-en.pdf))
- 2014 AGM minutes ([german PDF](2014-de.pdf))([english PDF](2014-en.pdf))
- 2013 AGM minutes ([german PDF](2013-de.pdf))
- 2012 AGM minutes ([german PDF](2012-de.pdf))
- 2011 AGM minutes ([german PDF](2011-de.pdf))([english PDF](2011-en.pdf))
- 2010 AGM minutes ([german PDF](2010-de.pdf))
- 2009 AGM minutes ([german PDF](2009-de.pdf))
- 2008 AGM minutes ([german PDF](2008-de.pdf))
- 2007 AGM minutes ([english](2007))
- 2006 meeting notes ([english](2006))
- 2005 meeting notes ([english](2005)) and working groups discussion ([english](2005-working-groups-discussion))
- 2004 meeting notes ([english](2004))
- 2003 meeting notes ([english](2003))
- 2002 meeting notes ([english](2002))
