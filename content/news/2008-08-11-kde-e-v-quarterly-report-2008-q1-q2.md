---
title: 'KDE e.V. Quarterly Report 2008 Q1/Q2'
date: 2008-08-11 00:00:00 
layout: post
---

The <a href="http://ev.kde.org/reports/ev-quarterly-2008Q1-Q2.pdf">KDE e.V.
Quarterly Report</a> is now available for January to June 2008. This document includes
reports of the board and the working groups about the KDE e.V. activities of the first two
quarters of 2008, and future plans.
