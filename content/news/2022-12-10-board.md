---
title: 'KDE e.V. board sprint'
date: 2022-12-10 10:00:00
layout: post
noquote: true
slug: board
categories: [Board]
---

The board of KDE e.V. is meeting in person in Berlin, Germany, over the weekend
of 10 and 11 December. Sticky notes for events, HR, and community have been
stuck to whiteboards, and the board will be meeting community members for
dinner on Saturday evening.
