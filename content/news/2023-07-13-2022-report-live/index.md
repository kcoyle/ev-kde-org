---
title: KDE's annual report for 2022 has landed.
date: 2023-07-12 14:30:00
images:
- screenshot.png
---

Read up on all stuff the Community did, the events we went to, how we used the funds generously donated by sponsors and the public, and much more.

[![2022 e.V. report](screenshot.png)](/reports/ev-2022)

[Link](/reports/ev-2022)