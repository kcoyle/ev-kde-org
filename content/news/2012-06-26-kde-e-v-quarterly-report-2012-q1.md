---
title: 'KDE e.V. Quarterly Report 2012 Q1'
date: 2012-06-26 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="http://ev.kde.org/reports/ev-quarterly-2012_Q1.pdf">first quarterly report of 2012</a>.

This report covers January through March 2012, including statements from the board, reports from sprints, financial information and a special part about the ALERT research project.
      
