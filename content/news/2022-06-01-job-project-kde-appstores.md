---
title: "KDE e.V. is looking for a developer to help further KDE's presence on app stores"
date: 2022-06-01 20:00:00
layout: post
noquote: true
slug: job-project-kde-appstores
categories: [Hiring]
---

Edit 2022-08-24: applications for this position **are closed**.

KDE e.V., the non-profit organisation supporting the KDE community, is looking for a someone to help KDE be present in different application stores.

We are looking for people who can start working on the projects soon, we expect this to be a part-time position.
Please see the [call for proposals for the App Stores project]({{ '/resources/callforproposals-appstores2022.pdf' | prepend: site.url }}) for more details about this contracting opportunity.

Looking forward to hearing from you.
