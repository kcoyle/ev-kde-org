---
title: 'KDE e.V. Quarterly Report 2013 Q2 and Q3'
date: 2013-12-09 00:00:00 
layout: post
---

KDE e.V., the non-profit organisation supporting the KDE community, is happy to present the <a href="http://ev.kde.org/reports/ev-quarterly-2013_Q2Q3.pdf">second and third quarterly report of 2013</a>.

This report covers April through September 2013, including statements from the board, reports from sprints and a feature about the research project ALERT.
      
