---
title: 'KDE e.V. Quarterly Report 2006Q4'
date: 2007-03-05 00:00:00 
layout: post
---

The <a href="http://ev.kde.org/reports/ev-quarterly-2006Q4.pdf">KDE e.V.
Quarterly Report</a> is now available for October to December 2006. It includes
reports of the board and the working groups about the KDE e.V. activities of the last
quarter of 2006 and future plans.
