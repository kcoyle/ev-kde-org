---
title: 'Akademy 2010 in Tampere, Finland'
date: 2009-09-02 00:00:00
categories: [Akademy]
---

Akademy 2010 will be held in Tampere, Finland, organized by
<a href="http://www.coss.fi/en/">COSS</a> and KDE e.V.

<p>
After a successful Akademy 2009 on the Canary Islands, as part of the Gran 
Canaria Desktop Summit, Akademy heads north to the birthplace of Linux where 
contributors will enjoy the midnight sun as they spend a week to present, plan 
and participate in the development of KDE software.
</p>

<p>
Akademy 2010 will be organized by COSS, the Finnish Centre for Open Source 
Solutions in cooperation with KDE e.V. <em>"We were happy to meet Mr. Ilkka 
Lehtinen at GCDS,"</em> says Sebastian K&uuml;gler, board member of KDE e.V.,
<em>"and we 
spent some time discussing what really goes into Akademy to make it such a 
vibrant event."</em> The conference is planned for late June or early July, and 
announcements will be made when the date is finalized and contributors to the 
Free Software desktop can start to make their summer plans.
</p>

<p>
A call for papers and participation is expected in the dark days of the year. 
Volunteers for the conference itself will be sought in the spring. Mr. Ilkka 
Lehtinen will lead the local team, Kenny Duffus will lead the KDE team and 
Claudia Rauch, Business Manager of KDE e.V., will be the central point
of contact and coordinate with the local team.
</p>

<p>
Cornelius Schumacher, president of KDE e.V., says of the coming conference, 
<em>"it will be great to show our technology in new areas and to reach out to
the  business community and Finland in particular with this Akademy. The release
of KDE 4.5 is scheduled around Akademy in 2010 and it will be a good moment to 
look back at the road from KDE 4.0 to stability, beauty and elegance."</em>
</p>

<h3>About KDE:</h3>
<p>
KDE is an international technology team that creates free and open source 
software for desktop and portable computing. Among KDE's products are a modern 
desktop system for Linux and UNIX platforms, comprehensive office productivity 
and groupware suites and hundreds of software titles in many categories 
including Internet and web applications, multimedia, entertainment, 
educational, graphics and software development. KDE software is translated 
into more than 60 languages and is built with ease of use and modern 
accessibility principles in mind. KDE4's full-featured applications run 
natively on Linux, BSD, Solaris, Windows and Mac OS X. 
</p>
