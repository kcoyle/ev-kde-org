<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/FOSDEM/KDE_FOSDEM.webp" alt="FOSDEM snd KDE logos with picture of Plasma Mobile." />
    </figure>
</div>


The [2022 edition of FOSDEM](https://fosdem.org/2022/) was held online from the 5th to the 6th of February. KDE has participated in FOSDEM for many years, and in 2021, KDE was one of the more popular booths, ranking third in the list of most visited virtual booths.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/FOSDEM/PlasmaMobile.jpg" alt="" />
        <figcaption>Bhushan Shah shows off features in Plasma Mobile on a Pinephone Pro.</figcaption>
    </figure>
</div>

In 2022, as we did in the prior edition, we organized demos at KDE's booth and kicked off Saturday with Bhushan Shah, who showed off the new features of Plasma Mobile. Claudio Cambra followed and explained Kalendar and the case for Akonadi, and Joseph De Veaugh-Geiss talked about KDE Eco.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/FOSDEM/Kalendar.jpg" alt="" />
        <figcaption>Clau Cambra gears up to explain Kalendar and defend Akonadi in his presentation at KDE's virtual booth.</figcaption>
    </figure>
</div>

On Sunday, by popular demand, Adam Szopa returned with his *Live KDE News* bulletin. The Promo team, along with Adam, gave a tour of Plasma 5.24 to the live audience. For our last demo, Carl Schwan did some live coding for Tokodon. Visitors were also able to grab some digital stickers for Matrix.

Overall, it was a fun weekend. On Saturday, 323 people signed in to the Matrix room and 357 people on Sunday. On average, between 150 to 170 people were watching the demos at any given time, and there was a lot of engagement from the audience.
