<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/Scale/booth.jpg" alt="Bhavisha stands in front of the booth at Scalex19." />
    </figure>
</div>

This year we were able to host the KDE booth at the 19th annual Southern California Linux Expo -- SCaLE 19X, that took place on July 28 - 31, 2022 at the Hilton Los Angeles Airport in LA, California.

<figure class="image-right">
    <img width="100%" src="images/Events/Scale/plasmamobile.webp" alt="A PinePhone showing the Plasma Mobile welcome screen." />
</figure>


[SCaLE ](https://www.socallinuxexpo.org/scale/20x)attracts visitors from all over the United States and around the world. It is the largest, community-run, open source, and free software conference in North America.

We collaborated with OpenSUSE, LOPSA and NextCloud to promote cross-community growth and to provide outreach and knowledge regarding our communities.

Our booth was appreciated for a lot of things but the main attraction was the banner. Attendees loved finding the logos of their favorite KDE tools. Our setup consisted of one big screen showcasing a promotional video of KDE, interactive laptops, devices with Plasma Mobile, and lots of Katie and Konqui stickers.

I was pleasantly surprised to see the excitement attendees felt when they saw us at the event. They wanted to know more about our products, and what was going on with the KDE community. People love experimenting with the Plasma Desktop and had fun trying out our applications. Plasma Mobile was also a hot topic of discussion and attendees were very eager to know the updates regarding it. Among the visitors to our booth, there were a lot of college students interested in joining our community and contributing to open source projects in general.

Attendees of all ages showed a keen interest towards our GCompris Application, with lots of kids enjoying playing educational games. One of the attendees was part of the education administration of a local school district and even expressed some interest in integrating the application as a beta at a few schools.

This was the first time we offered swag at our booth and the Katie and Konqui Stickers were a big hit in attracting attendees to our booth.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/Scale/stickers.webp" alt="Stickers of Katie, Konqi, and the KDE logo ont the KDE booth's table'." />
    </figure>
</div>

We went into this conference with a specific goal of outreach and spreading the word about KDE. I believe we succeeded quite a bit as we got a chance to work at a grass-roots level. We managed to uncover the next generation of open source contributors who are excited about the future of KDE.

This entire event would not have been possible without Drew Adams and the entire OpenSuse community. They played a huge role in helping us get a foothold at this conference. On behalf of all of KDE, we want to thank them and acknowledge all the hard work that they put into making this event happen.
