[Qt World Summit ](https://www.qt.io/qtws_2022)was held online on the 9th of November 2022. Qt World Summit is an event where you can hear the latest news on Qt technology, software tools and features and learn from other Qt users around the world.

Joseph De Veaugh-Geiss represented KDE and gave a talk on "KDE Eco: Achievements, Impact, and To-Do's". In this talk, Joseph talked about Okular receiving the Blue Angel eco-label and the research being carried out by KDE community members into methods of energy consumption measurements, setting up a community lab at KDAB Berlin, and developing tools for measuring software, among others.

The talk opened up opportunities for broader collaboration between Qt and KDE Eco, and provided practical examples for sustainability initiatives going forward.
