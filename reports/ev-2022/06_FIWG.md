2022 was a more financially eventful year for the organization than the previous COVID-19 pandemic years. After remaining fiscally conservative in 2020 and 2021 to keep the organization resilient in uncertain economic times, our goals for 2022 included not only overall growth in income and expenses, but also a carefully planned drawdown of our accumulated financial reserve in pursuit of our mission. This last goal had been on the organization's agenda for some time; the pandemic presented us with setbacks along the way. Starting in 2022, we have brought this goal back into focus.

Accordingly, we announced plans to return to in-person events and support travel (particularly around our flagship Akademy event) in 2022, and continued to execute on our staff growth strategy by adding positions. Our ability to execute to plan, from budgeting to execution, proved up to the challenge. The 19% increase in our total annual revenue to EUR 285,000 was within 1.5% of our goals and projections. The 76% increase in our total annual expenses to $385,000 - the first time in many years that KDE e.V. spent more than it earned, as we had intended - fell short of the extremely ambitious North Star goals we had set for ourselves (a total of $512,000). However, even here, the performance was much closer to plan than in previous years.

Digging deeper into the numbers, we saw modest revenue growth across all major categories - corporate donors, individual donors, and event sponsorships. Individual sponsorships continue to perform well, with a 16% increase in 2022 - additional individual fundraising opportunities, including the introduction of GitHub Sponsors for our code mirrors on the platform, contributed to this trend. Akademy sponsorships grew by 34%, largely due to increased pricing on sponsorship packages in line with the return to an in-person event format. Revenue from Google's student internship program continued to decline year over year.

On the expense side, our staff costs increased by 39% from 2021 to 2022, largely due to additional contracted positions, as well as inflationary adjustments and other developments in the compensation of our existing contractors. Our flagship event, the Academy, had a total cost of €72,000 in 2022 - a record for the conference due to a combination of unusually high venue costs and a travel support budget designed to signal and encourage the community's return to travel.

Taking stock of the first few months of the current year 2023, we continue to execute a very similar strategy. With additional contract positions now filled, we are slowly increasing our fundraising activity to sustain this volume over the long term. This was also started in 2022, with dedicated fundraising campaigns for subprojects such as Kdenlive, and a return of the KDE year-end fundraiser (all of which more than met their goals -- we continue to be very grateful to our many donors). Also in 2023, we were able to announce two additional corporate donors, Kubuntu Focus and g10 Code GmbH. Akademy, meanwhile, will aim to break even again in the coming years.

In addition to setting goals, we continue to improve our ability to predict and manage our financial journey. In 2023, the KDE e.V. Treasurer and Financial Working Group have already invested significant time and effort in upgrading the organization's financial toolchain, setting up new dashboards and preparing to provide access to sub-project teams to distribute budgeting and controlling to additional levels of the organization.


<table width="100%">
    <tr>
        <td style="vertical-align: top;">
            <table class="table">
                <thead>
                    <tr>
                        <th><h3>Income (€):</h3></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Patrons:</td>
                        <td class="text-right">67,000.00</td>
                    </tr>
                    <tr>
                        <td>Supporting members & donations:</td>
                        <td class="text-right">121,577.43</td>
                    </tr>
                    <tr>
                        <td>Akademy:</td>
                        <td class="text-right">40,225.75</td>
                    </tr>
                    <tr>
                        <td>Other Events:</td>
                        <td class="text-right">11,927.50</td>
                    </tr>
                    <tr>
                        <td>GSoC and Code in:</td>
                        <td class="text-right">4,496.43</td>
                    </tr>
                    <tr>
                        <td>Other</td>
                        <td class="text-right">40,268.86</td>
                    </tr>
                    <tr>
                        <td><b>Total Income:</b></td>
                        <td class="text-right"><b>285,495.97</b></td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td style="vertical-align: top;">
            <table class="table">
                <thead>
                    <tr>
                        <th><h3>Expenses (€):</h3></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Personnel:</td>
                    <td class="text-right" style="color: red">-209,465.82</td>
                </tr>
                <tr>
                    <td>Akademy:</td>
                    <td class="text-right" style="color: red">-68,774.97</td>
                </tr>
                <tr>
                    <td>Sprints</td>
                    <td class="text-right" style="color: red">-14,071.52</td>
                </tr>
                <tr>
                    <td>Other events:</td>
                    <td class="text-right" style="color: red">-14,275.11</td>
                </tr>
                <tr>
                    <td>Infrastructure:</td>
                    <td class="text-right" style="color: red">-12,202.23</td>
                </tr>
                <tr>
                    <td>Office:</td>
                    <td class="text-right" style="color: red">-8,233.56</td>
                </tr>
                <tr>
                    <td>Taxes and Insurance:</td>
                    <td class="text-right" style="color: red">-26,569.41</td>
                </tr>
                <tr>
                    <td>Other:</td>
                    <td class="text-right" style="color: red">-31,012.16</td>
                </tr>
                <tr>
                    <td><b>Total:</b></td>
                    <td class="text-right" style="color: red"><b>-384,604.78</b></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>

<table> <tr> <td> <img src="images/FiWG/fiwg_report_2022_piechart.png"/></td> </tr> </table>


