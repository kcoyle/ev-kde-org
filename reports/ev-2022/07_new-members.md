KDE e.V. welcomed the following new members in 2022:

* Claudio
* Emmanuel Charruau
* Xaver Hugl
* Timothée Ravier
* Phu Nguyen
* Tobias Berner
* Bharadwaj Raju
* Neal Gompa
