<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Projects/ghostwriter.png" alt="Screenshot of the distraction-free Ghostwriter text editor." />
    </figure>
</div>

KDE's new [Ghostwriter ](https://ghostwriter.kde.org/) app is a distraction-free text editor for Markdown. It features a live HTML preview as you type panel, theme creation, focus mode, fullscreen mode, live word count, and document navigation in an pleasingly simple writing environment.

Behind the scene, it is powered by the built-in`cmark-gfm` Markdown processor, and can integrate with Pandoc, MultiMarkdown, Discount, and cmark processors if they are installed.
