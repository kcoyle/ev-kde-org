<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/UbuntuSummit/groupphoto.jpg" alt="Ubuntu Summit group photo." />
    </figure>
</div>

From the 7th to the 9th of November, a bunch of KDE community members attended [Ubuntu Summit](https://events.canonical.com/event/2/) in the Czech Republic. The Ubuntu Summits stopped being held a decade ago, but have now been revived.

A group of KDE contributors were invited to this latest edition, and Adriaan "Ade" de Groot (maintainer of the Calamares installer), Scarlett Moore (Snap package builder), Aleix Pol i Gonzàlez (developer of the Discover software manager), Harald Sitter (KDE neon dev), Luca Weiss (OpenRazer dev that hangs around KDE) and me (who does KDE neon) took off to Prague.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/UbuntuSummit/kde.png" alt="KDE contributros at the Ubuntu Summit." />
    </figure>
</div>

Unlike the old Ubuntu Developer Summits, this wasn’t aimed at planning the next Ubuntu release, as they had already spent the last two weeks in the same hotel doing that. Instead, the event was a fun sharing of ideas, where people gave talks and workshops on what they were working on.

Scarlett and I gave a lightning talk on KDE Snaps and Scarlett extended our work by giving a workshop on the topic. KDE has over 100 apps in the Snap store, a great way to get KDE software promptly.

Ade gave a talk about his Calamares distro installer and compared it to Ubuntu’s installer which is being rewritten in Flutter. He also talked about KDE Frameworks. Harald gave talks on KDE neon and the secrets of KDE Plasma. Aleix spoke about the KDE community and what we do.

Adam Szopa works for KDE and also Canonical and he gave a talk on Linux gaming, Apparently, Canonical has a whole team just to get gaming working well.

It was great to catch up with Erich Eickmeyer who makes Ubuntu Studio and works for Kubuntu Focus selling laptops with Plasma. Ubuntu Studio ships with Plasma of course. I spoke to him about Wayland and he says the next release (for Ubuntu plus Plasma) is looking great for Wayland.

It was also great to meet Simon Quigley who does Lubuntu and has worked on Kubuntu. LxQt is a lightweight Linux desktop and probably one of the largest users of KDE Frameworks outside KDE. They use KScreen, KIdleTime, Solid, KWindowSystem and probably other Frameworks.

There was also a performance by Lorenzo’s Music, an open source band that create on Github using Ubuntu Studio and Kdenlive. They gave a great performance on the riverboat cruise under the Charles bridge in Prague.
