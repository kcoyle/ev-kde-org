<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Akademy-es/groupphoto_2000.jpg"
alt="Akademy-es 2023 group photo at OpenShouthCode." />
    </figure>
</div>


The eighteenth edition of
[Akademy-es](https://www.kde-espana.org/akademy-es-2021-en-linea), KDE’s yearly
event for the Spanish-speaking community, was held in Málaga from the 9th to the
10th of June. This year the event was jointly held with the OpenSouthCode event
and, like in previous years, attendees could participate both in person and
online.

For over 17 years, KDE España has been promoting and bringing Akademy-es to the
general public for Spanish-speaking community members.

During the two days, there were talks for both users and developers, as well as
practical workshops and other activities of a more social nature. KDE España
also set up a booth that ran demos of popular KDE applications, gave away
stickers and merchandising, and collected donations.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Akademy-es/booth_2000.jpg"
alt="Booth at Akademy-es 2023" />
    </figure>
</div>

The conference started with the opening ceremony hosted by Adrián Chaves,
president of KDE Spain. Then there were talks on various topics such as How KDE
project translations are managed, Python and Qt, The role of KDE in low-income
educational centres, Sustainability and free software/hardware, as well as
lightning talks and more.
