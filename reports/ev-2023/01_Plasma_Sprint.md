<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Sprints/Plasma/plasma_group.png" />
    </figure>
</div>

From the 5th to the 10th of May, the [Plasma
](https://kde.org/plasma-desktop/) team held the [Plasma 6
sprint](https://community.kde.org/Sprints/Plasma/2023) at  TUXEDO’s Office in
Augsburg, Germany.

The Plasma team discussed the transition to KF6 and its impact on dependency
management within the KDE framework. One significant change involved
restructuring colour scheme classes to reduce unnecessary dependencies on
`Qt::Widgets`, particularly for QML-based mobile applications. This
restructuring improves efficiency and reduces package size, especially for
Android APKs, which can account for up to 20-25% of the total size.

Led by David Redondo, the colour scheme code is now divided into the widget-less
`KF::ColorScheme` framework, with a small portion remaining in
`KF::ConfigWidgets` for generating colour scheme selection menus. Although
largely transparent to most consumers, this restructuring addresses the
dependency issue and optimises resource usage.

Aside from this, the Plasma team also discussed and worked on other KF6-related
topics, such as:

- the review of the XDG Portal Notification v2 proposal for compatibility with
`KF::Notification`, which promises significant improvements aligning with
cross-platform standards
- the integration of commonly used code pieces from applications to reduce
duplication and to streamline dependencies, such as an email address input
validator and a return key capture event filter for line edits
- the identification and resolution of test failures stemming from the CLDR 42
update, a part of the switch to Qt 6.5, which introduced unexpected changes in
certain time formats
- the ongoing refactoring of the `KF::Prison` barcode generator API to provide a
simpler interface.

These efforts collectively demonstrate the community's commitment to enhancing
the KDE framework for better performance and compatibility across different
platforms.
