<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Sprints/Promo/ima_648ee13.jpeg"
/>
    </figure>
</div>

On the 23rd and 24th of September, the Promo Sprint took place at the KDAB
office in Berlin, Germany, the KDE Promo team gathered to strategise and plan
various initiatives aimed at promoting KDE products and fostering diversity
within the community. within the community. Attendees included Aron (Online),
Aniqa, Carl, Joseph, Niccolo, Paul and Volker.

One of the main topics of discussion was the upcoming release of Plasma 6, KDE's
flagship desktop environment. Carl was tasked with updating the names on the the
Plasma 6 website, while Paul and Aniqa were in charge of handling download
names. Niccolo updated the Plasma 6 visuals, emphasising the importance of
having a and appealing visual identity. Joseph updated the team on the progress
of the visual blog posts, which are vital for engaging the community and
attracting new new users.

Video production for Plasma 6 was another key aspect discussed, with Niccolo and
Aron leading the discussion. The team outlined specific aspects to be covered
and and planned several videos to effectively showcase the features and
capabilities of Plasma 6. Plasma 6's features and capabilities. In addition,
Niccolo presented several slogan options for Plasma 6 options for Plasma 6,
soliciting feedback and ultimately selecting a compelling slogan that would
resonate with the target audience.

Both online and offline initiatives were explored for Plasma 6 events.
Discussions included organising online events such as AMAs on platforms such as
Discuss, YouTube and Peertube, as well as considering fireside chats to
encourage meaningful  interactions within the community. Aniqa and Joseph worked
together to promote Plasma 6 internally and planned release parties to generate
excitement, complete with Plasma 6-themed cakes and demonstrations in the
shopping centre.

Efforts have also been made to streamline event management processes, including
tracking and managing events effectively using available resources, and
Establishing clear objectives for each event. Aniqa was tasked with the creation
of a logistics/materials management system, while Carl used Gitlab to research
solutions to improve efficiency.

Diversity and inclusivity were recurring themes throughout the sprint, with
community-building initiatives and strengthening KDE's presence at FOSS events.
The team brainstormed strategies for specific demographics, identifying
potential partners and expanding the KDE networks to regions such as Africa,
Singapore and South Korea.

In addition to promoting diversity, the team also focused on measuring the
success of their initiatives and  and identifying target audiences for KDE
products, including students, teachers, gamers, embedded systems enthusiasts and
organisations. Standard content slides were developed to effectively introduce
KDE and FOSS concepts.
