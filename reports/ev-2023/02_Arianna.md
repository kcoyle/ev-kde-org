An ebook reader and library management app supporting ".epub" files, [Arianna
](https://apps.kde.org/en-gb/arianna/)automatically discovers your books and
sorts them by categories, genres, and authors. Like most open-source
applications, it is built on top of Qt and Kirigami. Arianna uses Baloo to find
your existing ePub files on your device and categorize them.

The library view tracks your reading progress and finds new books as soon as you
download them. If your book library is particularly large, you can use either
the internal search functionality or browse through various categories to find
books grouped by genre, publisher, or author. It displays the table of contents
of a book and provides metadata about your books. This includes support for
complex hierarchies of headings.

Arianna is also translated into multiple languages, thanks to some wonderful
translators. Here is the alphabetically sorted list: Basque, British English,
Catalan, Czech, Dutch, Finnish, French, German, Georgian, Hungarian,
Interlingua, Mandarin, Portuguese, Slovak, Spanish, Turkish, Ukrainian, and
Valencian.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img src="images/Projects/arianna.png"
alt="Arianna screenshot showing the library view" />
    </figure>
</div>
