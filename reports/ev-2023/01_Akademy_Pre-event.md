On 23 May 2023, the Akademy team year organized a hybrid event at the
University of Macedonia in Greece to help participants to know more about KDE
and Akademy.

The workshop explained how you can make a difference in the world of
free software by getting involved with the KDE Community. Nate Graham and
Neofytos Kolokotronis discussed KDE's vision and community structure, as well
as its impact on today's world. They also covered topics such as skill
development, career growth, volunteering, and personal growth.

Topics covered:

- What is KDE?
- How KDE has shaped today’s world?
- KDE’s continuing impact today
- How we got our start in KDE
- What can KDE do for you?
- Akademy

To know more about the event, you can [view the
slides](https://akademy.kde.org/media/2023/preevent_presentation.pdf) while
you watch the video:

<iframe title="KDE workshop at UOM: Making a Difference - 5/23/2023, 2:55:30
PM" width="560" height="315"
src="https://tube.kockatoo.org/videos/embed/aa2d5363-e281-46cb-a54f-
8b5a619bd6bd" frameborder="0" allowfullscreen="" sandbox="allow-same-origin
allow-scripts allow-popups allow-forms"></iframe>.
