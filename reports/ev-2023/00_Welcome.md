<figure class="image-right"> <img width="80%" src="images/Aleix.jpg"
alt="Picture of Aleix Pol" /> </figure>

As we close 2023, it's important to take
the time to reflect on our accomplishments as a community and organization. We
remained grounded in the present, delivering our software to our users, while we
also united towards our next technical iteration of products, rebasing them on
Qt 6 and getting them ready for the challenges to come.

This year saw the greatest progress yet towards achieving our current three KDE
goals. Under the *KDE for all* banner, we worked on accessibility, improving our
products for the people who would otherwise have difficulty using them. We also
took a look at our impact on our planet with the *Sustainable Software* goal and
worked towards better marrying technology to the future. And, last but not
least, we spent some quality time enhancing our internal processes, making sure
we work in the best of conditions.

In our community's upcoming gathering at Akademy, we will be voting for new KDE
Goals. These are changing times for software, especially for humans'
relationships with computers. We ought to embrace this new future in alignment
with our vision of "a world in which everyone has control over their digital
life and enjoys freedom and privacy", as its principles keep getting blurred at
the expense of our individual and collective freedoms.
