On the 22nd of June 1998, the [KDE Free Qt
Foundation](https://kde.org/community/whatiskde/kdefreeqtfoundation/) was
founded and has accompanied Qt on its amazing journey to become the success
story that it is today.

[Qt](https://www.qt.io/) has established itself as the go-to solution for UI
development because of its high quality, consistency, ease of use, and broad
cross-platform support. A key factor in this achievement is Qt's dual licensing
strategy: Qt is available as free software for open source, but it is also
available under a paid license for proprietary software development. A legal
foundation ensures the continued availability of Qt as free software alongside
the commercial licensing options.

At the time the KDE Free Qt Foundation was founded, Qt was developed by
Trolltech--the company that originally developed the framework. The Foundation
has supported Qt through its transitions, first to Nokia, then to Digia, and
finally to The Qt Company. It has the right to release Qt under the BSD license
if necessary to ensure that Qt remains open source. This remarkable legal
guarantee protects the free software community and creates trust among
developers, contributors, and customers.

The KDE Free Qt Foundation is a collaboration between The Qt Company and KDE.
KDE is one of the largest Free Software communities for general-purpose end-user
software, and has been around since 1996.

Qt is developed as a true open source project. People from many different
backgrounds join The Qt Company to contribute to the framework. Many
contributors come from The Qt Company, but many others come from other companies
and from Qt-based Free Software projects, including the KDE community. They know
that their contributions will continue to be available as Free Software because
the Foundation protects and ensures that contributions to Qt will remain open.

In 2023 we celebrated 25 years of freedom and collaboration for Qt!

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/Misc/KFQF.jpg"
alt="The KFQF celebrated 25 years in 2023." />
    </figure>
</div>

Find out more about how [KDE Free Qt
Foundation}Foundationhttps://kde.org/community/whatiskde/kdefreeqtfoundation/)
is protecting the future of Qt, both as open source and as a proprietary and
commercially supported offering.
