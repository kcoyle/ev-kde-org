<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Sprints/Kdenlive/team-in-zh.jpg" />
    </figure>
</div>

The [Kdenlive ](https://kdenlive.org/en/) team met in November 2023 in
Zürich for a sprint. Many topics were discussed, here is a quick overview of
what happened.

### Qt6 Strategy

The team tried to compile the KF6/Qt6 Kdenlive version on all supported
platforms. All upstream issues in dependencies have been solved, but this was
still an ongoing task by the end of the sprint as the code had not been built
with Qt6 on Mac at that point. The Windows and Linux versions started but
contained some subtle bugs, mostly qml/mouse related.

Several patches were submitted and merged to KDE Frameworks to fix
platform-specific issues. We postponed the decision of whether 24.02 would
be Qt6 based until mid-December.

### Effects Workflow

There were some in-depth discussions about how effects were to be displayed to
the user, how embedded effects (transform and volume) would be managed for
timeline clips, as well as some preliminary steps on how to move towards a more
"dope sheet like" management of keyframes. The proposed changes were:

- switch from a blacklist to a whitelist system, so that whenever a new effect
is implemented in a library, it won’t automatically appear in the UI, as many
effects don’t work without some additional layer. This would also allow us to
control better which effects are displayed. Of course, the user would be able
to disable this to see all available effects, for example, and be able to play
with new effects.
- in the effects list, add a link to the documentation website so that you can
quickly reach a page documenting each effect.
- start working on mock-ups for the embedded effects.

### Render Test Suite

The render test suite is a repository of scripts designed to check for
regressions in Kdenlive. Some refinements had still to be made to the report
UI, but the last missing steps to make it work were implemented during the
sprint.

### Group Behavior

Kdenlive did not used to behave very consistently when you have a group of
clips selected. For example, depending on how you did it, adding an effect would
sometimes be applied to one clip only or all clips in the group. Several other
operations had the same issues regarding groups. Several bugs were opened
related to this problem and, after the discussion, it was decided to make the
following changes:

- when operating on a grouped clip, all clips in the group would be affected
- we would implement selecting a single item in a group with a modifier (eg.
Ctrl+click). When such a selection were made, only the selected clip would be
affected.

### Bug/Issue Tracker

All remaining tasks on the deprecated Phabricator platform were closed, and
we decided to organise another online event to parse the GitLab issues to close
them all too, limiting Gitlab to the internal team communication/discussion.

Bugs and feature requests would be reported on the [KDE Bugtracker
platform](https://bugs.kde.org).

### Menu, Widgets and Misc

There were several complaints that rendering was not easy to find, and we
decided on the following:

- we would rename "Render" to "Export"
- we would move the "Render..."" and other import/export actions from the
"Project" menu to the "File" menu
- we would rename the "Project Bin" widget either to "Project" or to "Assets",
but no final decision was made
- added re-implement "Audio Spectrograph" and "Loudness Meter" from MLT to the
todo list
- we discussed which elements would be shown in a future Welcome Screen
- we added a basic check for offline update options by simply comparing the
version number with the current date, and suggesting the user upgrade if the
version is more than 6 months old.

### Licensing

The last steps to make the code fully REUSE compliant were carried out and a CI
job to prevent regressions was added.

### Roadmap

The website roadmap was reviewed and updated.

### Fundraising status

After the successful 2022 fundraising, the team started to use the
funds at last. The maintainer was able to spend a few hours more per week
working on Kdenlive thanks to the funds.

Regarding the tasks pledged in the fundraising proposal:

- the nested timelines feature was implemented
- improved keyframe easing modes were worked on, but without Bèzier curves
for the moment
- an improved effects workflow was also added to the short term roadmap.

### Public Events

We held a public in-person event where a couple of people showed up, which
was the occasion to demonstrate the basic editing workflow as well as a few
advanced features.

After that, an online meeting was the occasion to have some interesting
exchanges with the users.
