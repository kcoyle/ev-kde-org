[Francis ](https://apps.kde.org/en-gb/francis/)uses the well-known Pomodoro
technique to help himself become more productive.

The Pomodoro Technique is a time management method originally developed by
Francesco Cirillo in the late 1980s. It involves breaking work into intervals,
typically 25 minutes in length, separated by short breaks. This technique is
implemented in a Pomodoro app, which was originally developed by Felipe
Kinoshita.

The app is very simple and can serve as inspiration for developing your own
Kirigami application.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img src="images/Projects/francis.png"
alt="Francis, a pomodoro-based time-management app" />
    </figure>
</div>
