<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Projects/kde_eco_handbook.png" />
    </figure>
</div>

In 2023 KDE Eco published "[Applying The Blue Angel Criteria To Free Software:
A Handbook To Certify Software As Sustainable](https://eco.kde.org/handbook/)".

You might wonder: What does sustainability have to do with software at all? How
can something so seemingly immaterial as software have an environmental
footprint? We explored these and other questions in the handbook.

This handbook provides a brief overview of environmental harm driven by
software, and how the Blue Angel ecolabel—the official environmental label of
the German government—provides a benchmark for sustainable software design.

The Blue Angel is awarded to a range of products and services, from household
cleaning agents to small appliances to construction products. In 2020, the
German Environment Agency extended the award criteria to include software
products. It was the first environmental certification in the world to link
transparency and user autonomy—two pillars of Free & Open Source Software
(FOSS)—with sustainability.
