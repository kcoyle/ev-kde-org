The 2023 year was one of long awaited achievements that have been worked on for
some time.

In particular, we finally completed the retirement of some older website
infrastructure (known as Capacity) which had been used to run a number of our
sites, along with retiring a number of older Drupal instances. This has improved
the overall maintainability of our websites, and leaves just a small handful of
sites on Drupal 7 to be sorted in 2024. In addition to this, the server that
hosts the bulk of our public facing websites was also rebuilt, helping to ensure
the continued security of our infrastructure.
  
Gitlab also saw quite a bit of change this year, with us deploying both it's
container registry and pages modules. This container registry allows us to
better control distribution of the container images used in our CI system, while
Gitlab Pages allows our web team to preview website changes before they're
deployed, making it easier for people to get involved and easing review of
changes to our websites.
  
Looking ahead, further improvements to our CI system will be needed to improve
how unit tests are run, while continued server updates will be needed to ensure
we remain safe and secure - making a busy 2024.


* Created 36 subversion accounts
* Disabled 2 subversion accounts

---
* Created 3 [kdemail.net](http://kdemail.net/) aliases
* Modified 1 [kdemail.net](http://kdemail.net/) aliases

---
* Created 7 [kde.org](http://kde.org/") aliases
* Disabled 1 [kde.org](http://kde.org/) aliases
* Modified 5 [kde.org](http://kde.org/) aliases

---
* Created 3 [kde.org](http://kde.org/) mailing-lists: trojita kde-ev-supporters
privacy-reloaded
* Disabled 5 [kde.org](http://kde.org/) mailing-lists: kdepim-builds
gcompris-espanol khtml-cvs gcompris-portugues kde-dashboard
