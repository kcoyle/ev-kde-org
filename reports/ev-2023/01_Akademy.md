<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Akademy/00_group_photo_hall.jpg"
alt="Akademy 2023 group photo in hte main hall." />
    </figure>
</div>

[Akademy](https://akademy.kde.org/) is KDE's annual event where the community
comes together in one place to meet in person, share ideas and hack on common
projects.

This year's Akademy was held in Thessaloniki, Greece and started on July 15th
and ran until July 21st. This year 150 people attended Akademy in person, and
220 tuned in online to attend chats and BoFs over video conference.

The first weekend of Akademy, as is tradition, was dedicated to talks, panels
and fireside chats. The sessions, which were streamed live to the whole world,
covered a wide variety of KDE-related topics, ranging from the hot topic of the
road to Plasma 6, to how to hack off-the-shelf solar panels, and many things in
between.

### Day 1 - Saturday 15 July

##### 09:45 Opening

Aleix Pol, president of KDE, opened the event and thanked all attendees and
sponsors for making the event possible.

##### 10:00 Empowering Open-Source Space Technologies

The first keynote of Akademy 2023 was given by Eleftherios Kosmas from the Libre
Space Foundation. Eleftherios explained how the LSF is making inroads in space
exploration and technology through the use of a wide range of open source
technologies. He talked about how free software and hardware are carving out a
niche in the ultra-competitive and cut-throat space industry, despite the fact
that, as he reminded the audience several times, "space is hard".

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Akademy/Eleftherios.jpg"
alt="Eleftherios talks about the Libre Space Foundation in the opening key
note." />
    </figure>
</div>

##### 11:15 KDE Goals

In the traditional Goals time slot, Nate Graham, leader of the Automate and
Systematize Internal Processes goal, explained how they intend to preserve the
knowledge from one generation of KDE contributors to the next; Carl Schwan
listed the ways the Accessibility goal allows more people to use KDE software in
more ways than one; and Joseph De Veaugh-Geiss highlighted the milestones of the
KDE Eco goal.

At this point, the conference split into two tracks.

##### 12:30 Measuring Energy Consumption

In room 1, and related to the above, Volker Krause took us on a tour of how we,
the end users, can start contributing to greener computing by measuring
consumption at home. Volker walked us through the available software and then
told us about devices, some expensive, but some surprisingly cheap and
effective, that everyone could use.

##### 12:30 A Tale of Kleopatra

In room 2, Ingo Klöcker gave us a glimpse of how, over the course of a year, it
was possible to push KDE's cryptography key manager Kleopatra from barely
accessible to accessible with minor restrictions, often thanks to, sometimes in
spite of Qt.

##### 14:30 KDE e.V. Board Report

After lunch, the KDE e.V. board presented their annual report to the attendees.
The board members have done a lot of work to support the KDE community over the
past year. In this session, board members Adriaan de Groot, Aleix Pol Gonzalez,
Lydia Pintscher, and Nate Graham (Eike Hein couldn't make it) told the audience
about the things the e.V. has done, the work of the organization, and future
endeavors.

Topics covered included contractors (we have more of them now), events
(including external events), sprints, fundraising and sponsors, highlights of
the past year, and future plans.

##### 14:30 Over a million reasons why Snaps are important

In room 2, we heard from Scarlett Moore that there have been more than one
million downloads of Snaps since the project began in 2015. This means that Snap
is a packaging system to be reckoned with. Scarlett told us how she got into the
workflow of building a huge amount of Snaps and how she managed to keep them
updated using KDE's invent.kde.org platform and Ubuntu's launchpad.

##### 15:15 KDE e.V. Working Group reports

Back in room 1, we attended a panel hosted by Lydia Pintscher on KDE's working
groups. Neofytos Kolokotronis talked about the Community WG and how its main
mission is to act as a mediator, ensuring smooth communication between community
members and defusing conflicts. David Edmundson told us about the Finance WG and
how we had deliberately overspent in 2022 to reduce the accumulated funds in
KDE's coffers, and the ways the extra expenditure is being used to help
strengthen the community. Carl Schwan explained how the Fundraising WG has had
several recent successes, but continues to look for new opportunities. David
Redondo of the KDE Free Qt Foundation, responsible for protecting the free
version of Qt, told us about progress in the relationship between KDE and the Qt
Group. Finally, Bhushan Shah, representing the Sysadmin WG, talked about updates
to KDE's hardware infrastructure.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Akademy/workinggroups.jpg" alt="David
Edmundson, Neofytos Kolokotronis, Carl Schwan, Bhushan Shah, and Davi Redondo
telling us about the work carried out by the Working Groups." />
    </figure>
</div>

##### 15:15 Flatpak and KDE

At the same time, in room 2, Albert Astals talked about another way to
distribute software, this time using Flatpaks. In his talk, Albert discussed
what Flatpak is, why it's interesting for KDE, and talked about the different
ways developers can build and distribute their software using Flatpak, Flathub,
KDE's CI and binary factory infrastructures, and so on.

##### 16:25 KF6 - Are we there yet?

Later, in room 1, Alexander Lohnau, Nicolas Fella and Volker Krause talked about
the current state of KDE's frameworks and toolkits and the progress and
challenges of migrating to Qt 6.

##### 16:25 Documentation goals and techniques for KDE and open source

In room 2, Thiago Sueto discussed what it is to be a technical writer, what he
does, and what documentation goals and technologies are used to achieve them,
focusing on KDE in particular, but also on open source in general.

##### 17:05 Plasma 6 is coming

Marco Martin and Niccolò Venerandi then took the stage in room 1 and showed us
many of the new visual improvements we should expect to see in Plasma 6
(Niccolò) and the underlying technical parts, changing components and APIs
(Marco).

##### 17:05 UIs in Rust with Slint

At the same time in room 2, Tobias Hunger introduced us to Slint, a UI framework
written in Rust with bindings to Rust, C++, and Javascript. Slint scales from
microcontrollers with no OS and only a few KiB of RAM to desktop UIs supported
by graphics accelerators. The presentation showed what slint is and how to build
a small UI with it.

##### 17:55 - KRunner: Past, Present, and Future

In room 1, Alexander Lohnau talked about KRunner and how he started developing
for KDE three years ago, thanks in part to KDE's search-run-and-do-a-lot-more
utility. In his talk, Alexander covered the changes needed to migrate KRunner to
Plasma 6 and explored how to port and improve existing runners.

##### 17:55 - KDE Embedded - Where are we?

Meanwhile, in room 2, Andreas Cord-Landwehr talked about the tools KDE has for
easily creating embedded devices and how they work; which devices are most
interesting at the moment; the concept of an immutable image; and the next
topics and directions KDE community members should pursue in the embedded area.

### Day 2 - Sunday 16 July

##### 10:00 Kdenlive - what can we learn after 20 years of development?

Eugen Mohr, Jean-Baptiste Mardelle and Massimo Stella from the Kdenlive team
took us down memory lane, from the very beginning to the present day. They told
us about how the team came together, the hurdles they had to overcome, the
current situation and the plans for the future of KDE's popular video editing
software.

##### 10:50 Make it talk: Adding speech to your application.

Jeremy Whiting believes that speech is an underrepresented but perfectly valid
way to communicate with users. Jeremy proved his point with examples of
applications that, if speech-enabled, would help people who are visually
impaired (or just looking at something else) and went on to explain Qt
technologies that could be used to integrate speech into KDE applications.

##### 11:00 The Community Working Group - Keeping a Healthy Community

In this talk, Andy Betts presented the work of the Community Working Group, how
it resolves conflicts between community members and advice on how to get along
within the KDE community.

At this point, the conference split into two tracks.

##### 11:40 Internal Communication At KDE: Infrastructure For A Large And
Diverse Community

In room 1, Joseph De Veaugh-Geiss took the stage and walked us through some of
the problems that a community as large as ours has when it comes to
communicating with each other and the outside world.

##### 11:40 - An OSS Tool for Comprehending Huge Codebases

In room 2, Tarcisio Fischer talked about an OSS tool being developed by
CodeThink to help developers understand large codebases. Although the tool is
still under heavy development, he showed and explained the tool using KF5, Kate,
and Konsole as case studies.

##### 12:25 The Evolution of KDE's App Ecosystem and Deployment Strategy

Back in room 1, Aleix Pol explored how the Linux application ecosystem has
changed and the implications for KDE.

##### 12:25 Matrix and ActivityPub for everything

At the same time, in room 2, Alexey Rusakov, Carl Schwan, and Tobias Fella
talked about the Matrix IM platform and ActivityPub standards and how they can
be used outside of their primary purpose. They gave a broad overview of the
functionality provided by Matrix and how it fits into the ActivityPub protocols,
which are primarily intended for real-time messaging and social networking
applications.

During the lunch break, all attendees got together in the main conference hall,
and later in the University's lobby for the all-important group photo.

##### 14:30 Selenium GUI Testing

After lunch, in room 1, Harald Sitter told us about and demonstrated Selenium, a
technology for testing GUIs of KDE software. Selenium can be used for regular
testing, testing for accessibility problems, and also for energy efficiency.

##### 14:30 Remote Desktop for KWin Wayland

In room 2, Arjen Hiemstra showed how he was working to fix the lack of
networking support in Wayland.

In X11, running graphical applications remotely was a core feature. Wayland does
not implement this feature natively, but remote desktop control remains an
important use case for a number of users.

Arjen showed how it is possible to use KDE's KWin compositor, combined with a
new library called KRdp, to control any KWin Wayland session over a network.

<div> <video width="99%" autoplay loop muted> <source
src="images/Akademy/demo.mp4" type="video/mp4"> </video>
</div>


##### 15:15 Testing the latest KDE software

Later, in room 1, Timothée Ravier joined us virtually and discussed what, when
and how non-developers can test the latest KDE software, including Plasma
Desktop and Apps. He also gave a demo using Flatpak.

##### 15:15 Entering a Wayland-only World

Meanwhile, in room 2, Neal Gompa of the Fedora Project analyzed how KDE is doing
with its move to Wayland and what problems still need to be solved.

##### 16:20 KDE Wayland Fireside Chat

Right after that, Room 2 was handed over to Aleix Pol Gonzalez, David Edmundson,
David Redondo, Vlad Zahorodnii, and Xaver Hugl for a fireside chat with the
attendees about Wayland...

##### 16:20 Kyber: a new cross-platform high-quality remote control software

... And a round of 10-minute Fast Track talks began in room 1, with
Jean-Baptiste Kempf of VLC fame kicking things off with an explanation and demo
of his new project, Kyber.

Kyber attempts to provide a high quality, 0 latency video feed, with
bi-directional input forwarding, providing a cross-platform application and SDK
to control any type of machine, regardless of hardware and operating system.

##### 16:30 Fun with Charts: Green Energy in System Monitor

The second lightning talk was given by Kai Uwe Broulik, who explained how he
managed to get data from off-the-shelf solar panels (that originally used
proprietary software) and visualize it in Plasma's system monitor, alongside
things like CPU and network load.

##### 16:40 What has qmllint ever done for us?

Then Fabian Kosmale explained the usefulness of qmllint, a command-line utility
originally designed to check that QML files are syntactically valid. But it has
evolved to cover more checks, has been integrated into CI pipelines, and has a
plugin API on the way.

##### 16:50 Wait, are first-run wizards cool again?

Finally, Nate Graham introduced the new Plasma Wizard and explained why it was
necessary in this day and age.

Both tracks rejoined for the final leg of the conference section of Akademy.

##### 17:05 Sponsors lightning talks

Akademy would not be possible without the support of our sponsors. Aleix Pol
welcomed to the stage representatives from the Qt Group, Codethink, KDAB,
Canonical, Collabora, openSUSE, enioka Haute Couture and Externly. The
representatives, many of whom are also KDE members, explained what they do and
received a round of applause from the audience.

Other sponsors who were not present at the event, but who were also applauded by
the attendees, were Slimbook, TUXEDO, and PINE64.

The media partner for the event was Linux Magazine.

##### 17:50 Akademy Awards

The Akademy Awards recognize outstanding contributions to KDE. Aniqa Khokhar and
Harald Sitter, last year's winners, presented this year's winners:

In the category of Best Application, the winner was Heaptrack, maintained by
Milian Wolff and the rest of the development team.

The award for Best Non-Application Contribution went to Hannah Von Reth for her
work on KDE's *Craft* system.

The Jury Award went to Johnny Jazeix for his work organizing and supporting
*Season of KDE* and *Google Summer of Code*.

Finally, the special award went to the local team representing the University of
Macedonia, co-organizer of the event. The KDE community cannot thank the
University and the organizing crew enough for the work they put into making
Akademy 2023 a success.

##### 18:10 Closing

Aleix Pol closed the conference part of the event and the five days of BoFs,
hackathons and training began.

### Birds of a Feather Sessions

During the rest of the week, community members met in BoFs and worked on
advancing different aspects of their projects.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Akademy/BoF.jpeg"
alt="Developers at work during a BoF." />
    </figure>
</div>
