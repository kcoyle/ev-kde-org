<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/FOSDEM/happy_booth.jpg"
alt="The KDE team at FOSDEM 2023." />
    </figure>
</div>

After three long years of virtual gatherings, the Free and Open Source
Developers European Meeting ([FOSDEM](https://archive.fosdem.org/2023/))
returned to Brussels from the 4th to the 5th of February. KDE made a significant
impact at FOSDEM 2023. With a large team, the KDE community actively
participated across various tracks, forging connections with other projects and
communities. The KDE stand drew continuous crowds eager to learn about the
latest developments.

The KDE team actively engaged with attendees during the event, offering
insights into the latest projects and initiatives. Demonstrations of the Steam
Deck, RISC-V technology, and other innovations captivated audiences and sparked
meaningful conversations. The popularity of merchandise, including T-shirts and
caps, exceeded expectations, with proceeds contributing to the ongoing
development of KDE projects.

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/FOSDEM/booth.jpg"
alt="Photos showing the booth with devices and merch and attendees browsing." />
    </figure>
</div>

### Steam Deck

The unveiling of the Steam Deck, a consumer device preloaded with KDE software,
garnered significant attention, underscoring KDE's expanding influence beyond
traditional computing platforms.

### RISC-V and Yocto

One of the highlights at the KDE stand was the VisionFive-2 RISC-V board running
a Yocto-based system with a Plasma Bigscreen. This demonstration, spearheaded by
KDE developer Andreas, showcased the evolution of RISC-V technology within the
KDE ecosystem. What began as a niche endeavour in 2019 has now blossomed into a
remarkable showcase of innovation, with powerful demonstrations achievable on
affordable single-board computers.
