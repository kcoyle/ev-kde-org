### The Mighty 6 Migration

If there was one thing that the whole community rallied behind and worked on, it
was the effort to migrate the KDE software to new and future-proof foundations —
namely Qt6 and Wayland.

The migration covered not only Plasma, but also the frameworks on which other
KDE applications rely, a large number of those applications, widgets, and
finally add-ons.

KDE is a community with no defined management. Nevertheless, this self-motivated
group of people pulled together and accomplished this feat with no other
incentive than to provide better free software to their fellow humans.

### Discuss

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Highlights/discuss.png"
alt="KDE's new forum, Discuss" />
    </figure>
</div>


Although testing started in December 2022, KDE's new Discourse-based forum
[Discuss](https://discuss.kde.org) was only announced to the public on 4 April
2023

The result was electric. The old forum was great success in its time and had
become a veritable knowledge base, but the outdated interface had led to a
decline in traffic as users' tastes had evolved to expect a more modern and
interactive platform.

One year on, Discuss has around 7000 registered users who logged on 630,000
times between its opening and the end of the year.

Discuss also became a rich source of information for casual visitors, with 1.1
million visits to the various topics users have been talking about.

### KDE embraces the Fediverse

<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%"  src="images/Highlights/peertube.png"
alt="KDE's video channel on PeerTube" />
    </figure>
</div>

Concern has been growing for a number of years that the principles of
closed-source social media platforms do not align with those of KDE. As outlets
such as Twitter, Reddit and Facebook have become increasingly enshittified, the
community has stepped up its efforts to move away from them and adopt the
services offered by Fediverse.

Although it has only been running since 2018 (albeit originally on a different
instance), [KDE has become a force on Mastodon](https://floss.social/deck/@kde),
reaching over 20,000 followers in 2023 and adding an average of over 600 new
followers per month.

[KDE's Peertube instance](https://tube.kockatoo.org/c/kde/videos) has also come
into its own. Now KDE projects such as LabPlot, Krita, Kdenlive, and GCompris ­—
as well as individual developers — regularly upload tutorials, talks and vlogs
to the platform and have a healthy number of subscribers.

And when the owners of Reddit began to escalate anti-features in an attempt to
make the news aggregation platform more attractive to investors, the community
responded quickly by setting up a [KDE-themed Lemmy
instance](https://lemmy.kde.social/), providing a safe haven for users away from
speculators, AI data scrapers, and power-crazed billionaires.
