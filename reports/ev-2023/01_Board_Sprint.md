<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Sprints/Board/board-sprint-2023.png"
alt="From left to right: Eike, Nate, Aleix, Adriaan and Lydia." />
    </figure>
</div>

The Board members of KDE e.V. meet weekly online, but they also have
in-person
board meetings and sprints. In 2023, the board met in person in Berlin for the
Board Sprint.

The Board Sprint was held in Berlin, Germany, over the weekend of 27th and 28th
May 2023. All the Board members—Adriaan de Groot, Aleix Pol i Gonzàlez, Eike
Hein, Lydia Pintscher, and Nate Graham—met in person.

The agendas of these board meetings focus on various important topics including
events, HR, and community. These sprints are a chance for the Board members to
come together, discuss important matters, and socialize in person, which helps
to create a sense of community and enhance the overall functioning of KDE.

Furthermore, the board engaged with local community members over an Ethiopian
dinner on Saturday evening.
