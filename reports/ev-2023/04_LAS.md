<div style='text-align: center'>
    <figure style="padding: 1ex; margin: 1ex;">
        <img width="100%" src="images/Events/LAS/las2023brno_grouppohoto.jpg"
alt="LAS 2023 group photo." />
    </figure>
</div>


[Linux App Summit (LAS) ](https://linuxappsummit.org/)was held online and
in-person from the 21st to the 23rd of April in Brno, Czech Republic. KDE and
GNOME co-hosted the conference that brings the global Linux community together
to learn, collaborate, and help grow the Linux application ecosystem.

This year more than 140 people attended both in-person and online. Energised,
fervent, and hopeful, advocates of open-source technology from the GNOME and KDE
communities converged for a dynamic two-day event filled with creativity,
collaboration, and enjoyment.

The event included talks, panels and Q&As on a wide range of topics, including a
talk about "Regulatory state of play of Open Source in the EU" delivered by
Marcel Kolaja, Volker Krause talked about "UnifiedPush - Push notifications for
Linux", "Using XDG Desktop Portals for Qt apps" by Aleix Pol Gonzalez, and many
more.
