This year, KDE was represented at numerous events worldwide.

KDE contributors attended [Freedom Not Fear
](https://freedomnotfear.org/2023)from the 1st to the 3rd of September in
Brussels, and the [NextCloud Conference
](https://nextcloud.com/conference-2023/) from the 16th to the 17th of
September in Berlin. Volker Krause participated in the [OpenStreetMap Hack
Weekend
](https://wiki.openstreetmap.org/wiki/Karlsruhe_Hack_Weekend_September_2023)
from the 25th to the 26th of February in Berlin, focusing on using OSM data in
the KDE Itinerary. Volker also delivered a talk at the [FOSSGIS
Conference ](https://fossgis-konferenz.de/2023/) that was held from the 15th to
18th March in Berlin.

KDE contributors also attended Qt-related events and visited the [KDE + Qt
Meetup ](https://www.qt.io/blog/kde-qt-meetup-berlin-aug-10th) that was held on
the 10th August, and the [Qt Contributor Summit
](https://wiki.qt.io/Qt_Contributor_Summit_2023) held from the 30th of November
to 1st of December, both in in Berlin.

KDE hosted stalls and booths at various FOSS and non-FOSS events throughout the
year, including:

- [State of Open Con](https://stateofopencon.com/soocon-2023/) from the 7th to
the 8th February in London
- [SCaLE 20x](https://www.socallinuxexpo.org/scale/20x) from the 9th to the
12th March in Pasadena, California
- [Megacon](https://fanexpohq.com/the-fhq/event-highlights-megacon-orlando/) to
promote Krita from the 30th March to the 2nd of April in Orlando, Florida
- [FOSSASIA Summit](https://eventyay.com/e/7cfe0771), held from the 13th to
the 15th April in Singapore
- [Tübix](https://www.tuebix.org/), held on the 1st of July in Tübingen, Germany
- [LinuxDay Vorarlberg](https://linux-bildung.at/2023/09/linuxday-vorarlberg/),
held on the 30th of September in Dornbirn, Germany
- [FOSSCOMM](https://2023.fosscomm.gr/en/) from 21st to 22nd October in Voutes
- [37th Chaos Communication Congress
(37C3)](https://events.ccc.de/congress/2023/infos/index.html) held from the
27th to the 30th of December in Hamburg

Furthermore, KDE had a booth and talks by contributors at the [Qt World
Summit]()from the 28th to the 29th of November in Berlin.
