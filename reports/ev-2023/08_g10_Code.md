<figure class="image-right"> <img width="80%"
src="images/Sponsors/Werner_Koch_Portrait_01.2015-2.jpg"
alt="Werner Koch - CEO of g10" /> </figure>

[g10 Code GmbH](https://gnupg.com/)
joined the ranks of KDE patrons! g10 Code provides custom development,
enhancements, and audits of cryptographic software -- in particular for the
popular GnuPG encryption and digital signature tools.

"The KDE Community supports us in providing professionally-designedible desktop
software to our users in many different languages," stated CEO Werner Koch.
"While we consider KDE's KMail mail client to have the best GnuPG integration,
our main businesses case comes from Windows users in professional settings using
our [GnuPG VS-Desktop](https://gnupg.com/gnupg-desktop.html) product, which is
approved by Germany, the EU, and NATO for use with restricted documents. This
allowed us to change from [donation-based
development](https://gnupg.org/blog/20220102-a-new-future-for-gnupg.html). Our
free-of-charge distribution [Gpg4win](https://www.gpg4win.org/) has hundreds of
thousands of downloads per month and is used by NGOs, journalists, and most
"Tor-based" transactions. This is all only possible because we provide a
KDE-based user interface to GnuPG with KDE's Kleopatra app."

KDE e.V. President Aleix Pol Gonzalez said: "KDE has a well-established
reputation for prioritizing privacy and security. For end-users, implementing
effective security measures is important but also challenging. I'm looking
forward to working further with g10 towards building great cryptographic
solutions that are easy to adopt in organisations of all sizes as well as on our
individual systems."

g10 Code joins KDE e.V.’s other Patrons: Blue Systems, Canonical, Google,
Kubuntu Focus, Slimbook, SUSE, The Qt Company, and TUXEDO Computers to continue
to support Free Open Source Software and KDE development through KDE e.V.
