In *Highlights* we talk about the year's events we think will have a big impact on the future of the KDE project as a whole.

#### KDE Eco

[KDE Eco](https://eco.kde.org/) is important for several reasons. Firstly because it has the potential to become a trend-setter. By effectively starting the race to get a green seal of approval with Okular, it has established that the speed and features are not the only criteria to take into account when gauging the quality of a software product, but energy effectiveness must also be considered. Hence, we will hopefully see public institutions and companies factoring the energetic efficiency into their software purchases and, in consequence, all software providers look to make their products greener.

Secondly because, although it has the word "KDE" as part of the name, it is a project that aspires to encompass all Free Software<sup>*</sup>. As is customary for our community, all information we gather from our labs and experiments is made publicly available so others can learn from it. We share everything, from the solutions to optimise efficiency, to the tools we use to measure the before and after, and the results we obtain. We also actively encourage other Free Software projects, regardless of whether they are part of the KDE project, to join us, to go through the tests, and get certified too.

#### Valve's Steam Deck

We have known for some time that the lack of devices with Linux graphical environments pre-installed is the number one hurdle standing in the way of the mass adoption of FLOSS desktops.
Users don't usually pick their desktops, but have Windows thrust upon them instead.

That is why Valve's choice of [Plasma](https://kde.org/plasma-desktop/) for the [Steam Deck](https://www.steamdeck.com/en/tech) is so important for KDE.

Valve has a cult status within the gaming community, and gamers have steered clear of Linux in general in the past. By putting KDE's desktop into a gaming device that was sold out before it was even available, Valve has opened up Plasma to a demographic that will find the idea of using a free platform less intimidating in the future.

---

<sup>*</sup> Closed proprietary software by definition cannot be audited so won't be able to qualify for a green seal like that of Blauer Engel.
