<figure class="image-right">
  <img width="100%" src="images/Sprints/NeoChat_Sprint_3.jpg" alt="NeoChat attendees" />
  <figcaption>NeoChat developers (from left to right) Tobias, Nicolas and Carl.</figcaption>
</figure>

On the 16th October, we held an improvised NeoChat mini development sprint in a small hotel room in Berlin on the occasion of the 25th anniversary of KDE. In a good KDE tradition, Carl spent this time improving NeoChat settings. He ported both the NeoChat general settings and the specific room settings to the new Kirigami CategorizedSetting component. Tobias fixed a lot of papercuts and now the power level should be fetched correctly. We also show the number of joined users instead of joined+invited users in the room information pane, and the user search is now case insensitive. Nicolas focused on fixing our Android build by making the spellchecking feature compile on Android.
