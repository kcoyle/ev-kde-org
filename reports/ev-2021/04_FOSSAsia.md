FOSSASIA was held online from 13th to 21st March and we were participating for the first time. 

We had two sessions at the Summit: The first was in the exhibitor’s room and was an hour-long session. The second session was part of the main hall and we were allotted five minutes to talk but we ended up having around 15-20 minutes.

Approximately 35 attendees in our session and it turned out to be a good platform to attract new contributors and users from Asian countries.
