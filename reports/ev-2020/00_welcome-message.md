<figure class="image-right"> <img width="100%" src="images/Aleix.jpg" alt="Picture of Aleix Pol" /> </figure>

The year 2020 will no doubt remain in our collective memories as a special one. We saw struggle both on a personal level and as a society.

Being able to rely on a great community like KDE has been fantastic, and I am glad to reflect on KDE and how we have nurtured our technical aspects as well as the most human ones.

In KDE and Free Software communities, we have always taken pride in developing our products in a worldwide distributed manner. Many of us have been working from home for years, and our contributors are spread across different continents. But we also had to adapt to the real world. We held in-person meetings, and, obviously, [Akademy](https://akademy.kde.org/2021), our yearly conference, was also an in-person event.

In 2020, however, we had to hold Akademy online, as well as several sprints. It wasn't the same as being physically together like other years, but we made the most of it. We managed to keep the conversation alive, working together against all odds, and for that, I am very grateful to everyone who made it possible.

So, despite everything, KDE got a lot of work done over the year, work that today is already in production and being used around the world on a daily basis. Our applications and systems got released, as did some KDE-based hardware, including laptops and even a phone. It's the latter that I see as the biggest irony, in that 2020 will be the year in which KDE and the overall Linux community finally got together and started working towards a polished experience for mobile users. I am excited to see such relevant products available for our community to build upon.

Be it for mobile phones, laptops or desktops, our work continues. We will be glad to have all of you over for a great 2021. Remember to join our goals [All About the Apps](https://community.kde.org/Goals/All_about_the_Apps), [Wayland](https://community.kde.org/Goals/Wayland) and [Consistency](https://community.kde.org/Goals/Consistency) and help bring Free Software to the next generation.

Looking forward to a 2021 that will surprise us in more delightful ways. See you all in the community.
