<div style='text-align: center'> <figure style="padding: 1ex; margin: 1ex;"><a href="images/PlasmaSprint/GroupPhoto.jpeg"><img src="images/PlasmaSprint/GroupPhoto.jpeg" width="100%" /></a><br /><figcaption>Plasma developers hard at work during the sprint. Top from left: Bhushan Shah, Aleix Pol, Arjen Hiemstra, David Edmundson, David Redondo, Kai Uwe Broulik, Marco Martin and Nicolas Fella.</figcaption></figure> </div>

From the 11th to the 14th of June, the [Plasma](https://kde.org/plasma-desktop/) team held their [annual sprint](https://community.kde.org/Sprints/Plasma/2020Virtual). Due to the COVID19 pandemic, we had to cancel our original week-long, in-person meet up scheduled for the end of April in Augsburg and hosted by our friends at [TUXEDO](https://www.tuxedocomputers.com), and instead had to settle for an online sprint. While four days of an online event can't fully replace an entire week in a room elbow-to-elbow with the most talented and dedicated people you know, hacking and discussing stuff from 9 till midnight online turned out to be surprisingly productive.

During the sprint, the Plasma team discussed and worked on:

* [KDE's GitLab migration](https://dot.kde.org/2020/06/30/kdes-gitlab-now-live) plans and timeline
* The Future of the [plasma-framework](https://api.kde.org/frameworks/plasma-framework/html/libplasma.html) in KF6
* Widget and Plasma styling in Plasma 6
* Various [Wayland](https://wayland.freedesktop.org/) features like clipboard, virtual keyboard, screencasting, layer shell, tiling for tablet mode and implementation details for them
* Improving experience for new contributors and outreach
* Sound theme support using libcanberra in Plasma and [knotifications](https://api.kde.org/frameworks/knotifications/html/index.html)
* Notifications on lockscreen for Plasma Mobile

... among many other things.